#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>
#include <QDir>
#include "azimuth_qt_header.hpp"
#include <QMessageBox>
#include <QTextStream>
#include <QSerialPort>
#include <QThread>
#include <QDebug>
#include <QSerialPort>
#include <cstring>
#include <QTextStream>
#include <QTextCodec>
#include "QtConsole_/include/qconsole.h"
#include <QDateTime>
#include "clsthreadstate.h"
#include "QTimer"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:

     void updateHandle(QString state);
     void gpsHandle(QString state);
     void do_it();

private slots:
    void on_cmd_KKT_clicked();

    void on_cmd_2dCode_clicked();

    void on_cmd_Transitek_clicked();

    void on_cmd_sound_clicked();

    void on_cmd_internet_clicked();

    void on_cmd_backMain_clicked();

    void on_cmd_exit_clicked();

    void on_cmd_sysInfo_clicked();

    void on_cmd_asseler_clicked();

    void on_cmd_bluetoth_clicked();

    void on_cmd_internet_2_clicked();

    void on_cmd_record_clicked();

    void on_cmd_gps_clicked();

    void on_cmb_light_activated(const QString &arg1);

    void on_cmb_light_activated(int index);

    void on_cmd_wifi_clicked();

    void on_cmd_modem_clicked();

    void on_cmd_scanKKT_clicked();

    void on_cmb_kkt_activated(int index);

    void on_cmb_manager_internet_activated(int index);

    void on_cmb_manager_internet_activated(const QString &arg1);

    void on_cmd_battery_clicked();

    void on_cmd_lighten_clicked();


    void on_cmd_UpdateAcceler_clicked();

    void on_cmd_fullLight_clicked();

    void on_cmb_fonarik_activated(int index);

    void on_cmd_exit_2_clicked();

    void on_cmd_power_clicked();

    void on_cmd_power_back_clicked();

    void on_cmd_functionButton_clicked();

    void on_checkBox_transitek_stateChanged(int arg1);

    void on_checkBox_4g_stateChanged(int arg1);

    void on_checkBox_wifi_stateChanged(int arg1);

    void on_cmd_connect_wifi_clicked();

    void on_cmd_wifi_connect_clicked();

    void on_cmd_back_tab5_clicked();

    void on_cmd_key_0_clicked();

    void on_cmd_key_1_clicked();

    void on_cmd_key_2_clicked();

    void on_cmd_key_3_clicked();

    void on_cmd_key_4_clicked();

    void on_cmd_key_5_clicked();

    void on_cmd_key_6_clicked();

    void on_cmd_key_7_clicked();

    void on_cmd_key_9_clicked();

    void on_cmd_key_8_clicked();

    void on_cmd_save_tab5_clicked();

    void on_cmd_key_del_clicked();

    void on_cmd_key_cap_look_clicked();

    void on_cmd_key_Q_clicked();

    void on_cmd_key_W_clicked();

    void on_cmd_key_E_clicked();

    void on_cmd_key_R_clicked();

    void on_cmd_key_T_clicked();

    void on_cmd_key_Y_clicked();

    void on_cmd_key_U_clicked();

    void on_cmd_key_I_clicked();

    void on_cmd_key_O_clicked();

    void on_cmd_key_P_clicked();

    void on_cmd_key_A_clicked();

    void on_cmd_key_S_clicked();

    void on_cmd_key_D_clicked();

    void on_cmd_key_F_clicked();

    void on_cmd_key_G_clicked();

    void on_cmd_key_H_clicked();

    void on_cmd_key_J_clicked();

    void on_cmd_key_K_clicked();

    void on_cmd_key_L_clicked();

    void on_cmd_key_Z_clicked();

    void on_cmd_key_X_clicked();

    void on_cmd_key_C_clicked();

    void on_cmd_key_V_clicked();

    void on_cmd_key_B_clicked();

    void on_cmd_key_N_clicked();

    void on_cmd_key_M_clicked();

    void on_checkBox_SSH_stateChanged(int arg1);

    void on_cmd_key_dot_clicked();

    void on_cmd_key_space_clicked();

    void on_cmd_key_min_clicked();

    void on_cmd_key_dwoe_clicked();

    void on_cmd_delete_wifi_clicked();

    void on_cmd_mem_sleep_clicked();

    void on_checkBox_USB_stateChanged(int arg1);

    void on_checkBox_KKT_On_stateChanged(int arg1);

    void on_checkBox_Led_Green_stateChanged(int arg1);

    void on_checkBox_Led_Red_stateChanged(int arg1);

    void on_checkBox_bootloader_mode_stateChanged(int arg1);

    void on_checkBox_led_stateChanged(int arg1);

    void on_cmd_date_time_back_clicked();

    void on_cmd_date_time_kkt_clicked();

    void on_cmd_date_time_terminal_clicked();

    void on_cmd_paint_clicked();

    void on_cmd_send_OFD_clicked();

    void on_cmd_date_time_1_clicked();

    void on_cmd_date_time_2_clicked();

    void on_cmd_date_time_3_clicked();

    void on_cmd_date_time_4_clicked();

    void on_cmd_date_time_5_clicked();

    void on_cmd_date_time_6_clicked();

    void on_cmd_date_time_del_clicked();

    void on_cmd_date_time_7_clicked();

    void on_cmd_date_time_8_clicked();

    void on_cmd_date_time_9_clicked();

    void on_cmd_date_time_0_clicked();

    void on_cmd_date_time_dot_clicked();

    void on_cmd_date_time_wdot_clicked();

    void on_cmd_date_time_terminal_2_clicked();

    void on_cmd_date_time_del_2_clicked();

    void on_cmd_date_time_kkt_2_clicked();

    void on_cmb_fonarik_activated(const QString &arg1);

    void on_cmdKKT_power_low_clicked();

    void on_cmdPowerOffYes_clicked();

    void on_cmdPowerOffNo_clicked();

private:
    Ui::MainWindow *ui;
    //QConsole *cons;
    void showInfo(QString txt);
    void waitShow(QString txt = "ПОЖАЛУЙСТА, \n\r ПОДОЖДИТЕ.");
    void waitHide(int nextUI = 1);
    QThread *threadState;
    clsThreadState *stateThr;
    clsThreadGPS *stateGPS;
    qint8 verPowerBoard = 0;
    QString port_barcode_scan = "";
    QString port_kkt = "";


    QProcess pr_mom_acs;
    QProcess pr_mod_gps;
    QProcess pr_mod_term;

    QString shell_command(const char *input, int timeout = -1, char isErrorRes = 0x00);
    void addLog(QString txt);
    QString gps_string;

    void addTextWiFi(QString key);
    void addTextDiteTime(QString key);

    void usbHUBEnable(bool enable);

    bool capKeyBoard;

    bool chek_gpio(QString cmd);


};
#endif // MAINWINDOW_H
