#include "mainwindow.h"
#include "ui_mainwindow.h"

extern QTextCodec *cp1251;
extern QTextCodec *utf8;
extern QTextCodec *cp866;
extern QTextCodec *cpcurrent;
extern QString tempqstr;
extern QTextStream tempqstream;

//echo 0 > /sys/class/leds/gsm_pwr_key/brightness # pwrbutton not pressed
//echo 0 > /sys/class/leds/gsm_usb_boot/brightness # normal boot
//sleep 1
//echo 1 > /sys/class/leds/gsm_ena_gsmpwr/brightness #power on

enum PageWindows
{
    pageMain,
    pageInfo,
    pageWait,
    pagePower,
    pageWiFi,
    pageTime,
    pageExit
};

void MainWindow::do_it()
{

    ui->lblPIC->setVisible(false);

    QString ver1 = shell_command("cat /sys/class/i2c-dev/i2c-2/device/2-0040/charger_board_ver"); //# > 1.0
    QString ver2 = shell_command("cat /sys/class/i2c-dev/i2c-2/device/2-0010/charger_board_ver"); //# > 2.0

    if(ver1.indexOf("No such file or directory") > 0)
    {
        verPowerBoard = 2;
        ui->cmb_kkt->setCurrentIndex(2);
    }
    else
    {
        verPowerBoard = 1;
        ui->cmb_kkt->setCurrentIndex(0);
    }

    if(ver2.indexOf("No such file or directory") > 0)
    {
        verPowerBoard = 1;
        ui->cmb_kkt->setCurrentIndex(0);
    }
    else
    {
        verPowerBoard = 2;
        ui->cmb_kkt->setCurrentIndex(2);
    }

    // /dev/ttySC0 -> /dev/ttymxc2
    port_barcode_scan = "/dev/ttymxc2";
    port_kkt = "dev/ttymxc2";

//    ui->cmb_fonarik->setEnabled(false);

//    ui->lblCharger->setVisible(false);

//    ui->pic_Battery->setVisible(false);

    ui->cmd_2dCode->setEnabled(true);

    ui->cmb_fonarik->setEnabled(true);
    ui->cmb_fonarik->setCurrentText("115200");

    //ui->cmd_functionButton->setEnabled(false);

    //ui->cmd_bluetoth->setEnabled(false);

    //ui->cmd_record->setEnabled(false);

    QString command = "monitor-sensor";

    ui->cmd_UpdateAcceler->setVisible(false);

    QString isOkKKTpower = shell_command("cat /sys/class/leds/charger_board_kkt_power/brightness");
    if(isOkKKTpower.trimmed()== "0")
    {
        ui->checkBox_KKT_On->setChecked(false);
        ui->cmd_KKT->setEnabled(false);
        ui->cmd_scanKKT->setEnabled(false);
        ui->cmdKKT_power_low->setEnabled(false);
        ui->cmd_send_OFD->setEnabled(false);
        ui->cmd_date_time_kkt->setEnabled(false);
        ui->cmd_date_time_kkt_2->setEnabled(false);
        ui->cmd_date_time_kkt_2->setEnabled(false);


    }
    else
    {
        ui->checkBox_KKT_On->setChecked(true);
        ui->cmd_KKT->setEnabled(true);
        ui->cmd_scanKKT->setEnabled(true);
        ui->cmdKKT_power_low->setEnabled(true);
        ui->cmd_send_OFD->setEnabled(true);
        ui->cmd_date_time_kkt->setEnabled(true);
        ui->cmd_date_time_kkt_2->setEnabled(true);

    }

    QString isOnBSK = shell_command("cat /sys/class/leds/ena_bskpwr/brightness");

    if(isOnBSK.trimmed() == "0")
    {
        ui->cmb_fonarik->setEnabled(false);
        ui->cmd_Transitek->setEnabled(false);
    }
    else
    {
        ui->cmb_fonarik->setEnabled(true);
        ui->cmd_Transitek->setEnabled(true);
    }


    // shell_command("echo 0 > /sys/class/leds/gsm_ena_gsmpwr/brightness");
    QString isGPS = shell_command("cat /sys/class/leds/gsm_ena_gsmpwr/brightness");

    if(isGPS.trimmed() == "0")
    {
        ui->checkBox_4g->setChecked(false);
    }
    else
    {
        ui->checkBox_4g->setChecked(true);
    }

    QString isKKTusb = shell_command("cat /sys/class/leds/charger_board_kkt_connect/brightness");

    if(isKKTusb.trimmed() == "0")
    {
        ui->checkBox_USB->setChecked(false);
    }
    else
    {
        ui->checkBox_USB->setChecked(true);
    }

    QString isLEDgreen = shell_command("cat /sys/class/leds/charger_board_led_green/brightness");

    if(isLEDgreen.trimmed() == "0")
    {
        ui->checkBox_Led_Green->setChecked(false);
    }
    else
    {
        ui->checkBox_Led_Green->setChecked(true);
    }

    QString isLEDred = shell_command("cat /sys/class/leds/charger_board_led_red/brightness");

    if(isLEDred.trimmed() == "0")
    {
        ui->checkBox_Led_Red->setChecked(false);
    }
    else
    {
        ui->checkBox_Led_Red->setChecked(true);
    }

    QString isBootMode = shell_command("cat /sys/class/leds/charger_board_bootloader_mode/brightness");

    if(isBootMode.trimmed() == "0")
    {
        ui->checkBox_bootloader_mode->setChecked(false);
    }
    else
    {
        ui->checkBox_bootloader_mode->setChecked(true);
    }

    shell_command("echo 1 > /sys/class/leds/charger_board_led_power/brightness");
    shell_command("echo 0 > /sys/class/leds/charger_board_led_power/brightness");
    QString isLEDon= shell_command("cat /sys/class/leds/charger_board_led_power/brightness");

    if(isLEDon.trimmed() == "0")
    {
        ui->checkBox_led->setChecked(false);
    }
    else
    {
        ui->checkBox_led->setChecked(true);
    }

    //systemctl is-enabled ssh
    QString isSSH = shell_command("systemctl is-enabled ssh");

    if(isSSH.trimmed() == "enabled")
    {
        ui->checkBox_SSH->setChecked(true);
    }
    else
    {
        ui->checkBox_SSH->setChecked(false);
    }

    QString isWIFI = shell_command("nmcli radio  wifi");

    if(isWIFI.trimmed() == "enabled")
    {
        ui->checkBox_wifi->setChecked(true);
    }
    else
    {
        ui->checkBox_wifi->setChecked(false);
    }

    pr_mom_acs.start("/bin/sh", QStringList() << "-c" << command);

    pr_mod_gps.start("/bin/sh", QStringList() << "-c" << "/root/init_modem.sh");

    // set keyboard CAPSLOCK
    capKeyBoard = true;

    on_cmb_light_activated(70);

    QString backlight = "echo  70 > /sys/class/backlight/backlight-dsi/brightness";

    shell_command(backlight.toLatin1());

//    shell_command("/root/chg.sh");

//    shell_command("/root/chg.sh");

//    shell_command("/root/chg.sh");

    //shell_command("echo 1 > /sys/class/leds/ena_bskpwr/brightness");

    //shell_command("echo 1 > /sys/class/leds/ena_bskpwr/brightness");

    //shell_command("echo 1 > /sys/class/leds/ena_bskpwr/brightness");

    //clsThreadState->started();

    //threadState = new QThread;
    stateThr = new clsThreadState("B");
    //stateThr->moveToThread(threadState);
    connect(stateThr, SIGNAL(send(QString)), this, SLOT(updateHandle(QString)));
    //connect(threadState, SIGNAL(started()), stateThr, SLOT(doWork()));
    stateThr->start();
    //threadState->start();

     stateGPS = new clsThreadGPS("0");
     connect(stateGPS, SIGNAL(send(QString)), this, SLOT(gpsHandle(QString)));
     stateGPS->start();

     ui->tab_Main->setEnabled(true);
}

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    cpcurrent = cp1251;

    ui->tab_Main->tabBar()->hide();
    ui->tab_Main->setEnabled(false);

    QTimer::singleShot(500, this, SLOT(do_it()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::chek_gpio(QString cmd)
{
    QString isOnBSK = shell_command(cmd.toLatin1());

    bool res = false;

    if(isOnBSK.trimmed() == "1")
    {
        res = true;
    }
    else
    {
        res = false;
    }

    return res;
}

void MainWindow::showInfo(QString txt)
{
    ui->txtText->clear();
    ui->txtText->setText(txt);
}

void MainWindow::updateHandle(QString state)
{
   QStringList text= state.split(";");
   if(text.length() == 5)
   {
       // BT
       if(text[0] == "1")
       {
            ui->lblBT->setStyleSheet("background-color: rgb(138, 226, 52)");
       }
       else
       {
            ui->lblBT->setStyleSheet("background-color: rgb(239, 41, 41)");
       }

       // WI-FI
       if(text[1] == "1")
       {
            ui->lblWI->setStyleSheet("background-color: rgb(138, 226, 52)");
       }
       else
       {
            ui->lblWI->setStyleSheet("background-color: rgb(239, 41, 41)");
       }

       // 4G
       if(text[2] == "1")
       {
            ui->lbl4G->setStyleSheet("background-color: rgb(138, 226, 52)");
       }
       else
       {
            ui->lbl4G->setStyleSheet("background-color: rgb(239, 41, 41)");
       }

//       // GPS
//       if(text[3] == "1")
//       {
//            ui->lblGPS->setStyleSheet("background-color: rgb(138, 226, 52)");
//       }
//       else
//       {
//            ui->lblGPS->setStyleSheet("background-color: rgb(239, 41, 41)");
//            // всегда зеленый
//            ui->lblGPS->setStyleSheet("background-color: rgb(138, 226, 52)");
//       }

       ui->lbl_DT->setText(text[4]);

   }
   else
   {
       ui->lbl_DT->setText(state);
   }
}


void MainWindow::gpsHandle(QString state)
{
    gps_string = state;
}

void MainWindow::addLog(QString txt)
{
    QFile file("/root/mk100/mk100t/log.txt");

    if(file.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text))
    {

      QTextStream stream(&file);

      stream << QDateTime::currentDateTime().toString() << txt;

      file.close();
    }
}

QString MainWindow::shell_command(const char *input, int timeout, char isErrorRes)
{
  QProcess p;

  QString command = QString::fromLatin1(input);

  p.start("/bin/sh", QStringList() << "-c" << command);
  //p.start("/bin/bash", QStringList() << "-c" << command);

  //p.waitForFinished(timeout);

  if(timeout == -1)
  {
    p.waitForFinished(timeout);
  }
  else
  {
//    QThread::sleep(timeout);
//    p.terminate();
//    p.kill();
      p.waitForFinished(timeout);
  }

  qDebug() << "run" << command;

  auto res = p.readAllStandardOutput();
  auto err = p.readAllStandardError();

  qDebug() << "out" << res;

  qDebug() << "err" << err;

  addLog(res + "\r\n" + err);

  if(isErrorRes == 1)
  {
        err = "";
  }

  return res + "\r\n" + err;

}

void MainWindow::waitShow(QString txt)
{
    ui->lblWait->setText(txt);
    ui->tab_Main->setCurrentIndex(2);
    qApp->processEvents();
    this->setEnabled(false);
    qApp->processEvents();
    ui->txtText->setText("");
    qApp->processEvents();
}

void MainWindow::waitHide(int nextUI)
{
    qApp->processEvents();
    this->setEnabled(true);
    ui->tab_Main->setCurrentIndex(nextUI);
    qApp->processEvents();
    ui->cmd_UpdateAcceler->setVisible(false);
}
void MainWindow::on_cmd_internet_2_clicked()
{

}

void MainWindow::on_cmd_KKT_clicked()
{
    waitShow();

    //QString txt = "";

    //char oprname[100];
    int8_t isprint = 0;
    char emptystr[3];
    emptystr[0] = 0;
    char addreq[100] ; //=  "Доп. информация";

    int32_t retval;
    QString txt_info = "";
    char oprname[30] ; //s= "Иванов И. И.";
    char pswd[] = "AERF";

    char devname1[] = "/dev/ttySC0";
    char devname2[] = "/dev/rfcomm0";
    char devname3[] = "/dev/ttymxc2";

    char baudrate[] = "115200";
    int32_t flagoem = 1;

    if(flagoem == 0) {
        cpcurrent = cp866;
    }
    else {
        cpcurrent = cp1251;
    }

    tempqstr = "Иванов И. И.";
    std::strncpy(oprname, cpcurrent->fromUnicode(tempqstr).constData(), 30);

    tempqstr = "Доп. информация";
    std::strncpy(addreq, cpcurrent->fromUnicode(tempqstr).constData(), 30);

    qApp->processEvents();

    if(ui->cmb_kkt->currentIndex() == 0)
    {
        retval = OpenDLLPlus(oprname, pswd, devname1, baudrate, flagoem);
        //retval = OpenDLLPlus(oprname, pswd, port_kkt.toLatin1().data(), baudrate, flagoem);
    }
    else if(ui->cmb_kkt->currentIndex() == 2)
    {
        retval = OpenDLLPlus(oprname, pswd, devname3, baudrate, flagoem);
    }
    else
    {
        retval = OpenDLLPlus(oprname, pswd, devname2, baudrate, flagoem);

    }

    txt_info += "OpenDLLPlus: " + QString::number(retval) + "\r";

    if(retval != 0)
    {
        showInfo(txt_info);
        waitHide();
        return;
    }

    StartSeans();


    int32_t ShiftState = 0;
    int32_t ShiftTime = 0;

    retval = GetShiftState(&ShiftState, &ShiftTime);

    //outaddline(utf8->toUnicode("Штрих-код: "), tempstr);
//    qDebug() << "Debug ShiftTime" << QString::number(ShiftTime);
//    qDebug() << "Debug ShiftState" << QString::number(ShiftState);
    txt_info += "GetShiftState: " + QString::number(retval) + "\r";
    txt_info += "ShiftState: " + QString::number(ShiftState) + "\rShiftTime: " + QString::number(ShiftTime) + "\r";

    retval = XReport();
    txt_info += "XReport: " + QString::number(retval) + "\r";

    if(retval != 0)
    {
        retval = CloseDLL();
        txt_info += "CloseDLL: " + QString::number(retval) + "\r";

        showInfo(txt_info);
        waitHide();
        return;
    }

    retval = ShiftOpenPlus(oprname, emptystr, emptystr, addreq);
    txt_info += "ShiftOpenPlus: " + QString::number(retval) + "\r";

    QThread::sleep(1);

    retval = GetShiftState(&ShiftState, &ShiftTime);

    //outaddline(utf8->toUnicode("Штрих-код: "), tempstr);
//    qDebug() << "Debug ShiftTime" << QString::number(ShiftTime);
//    qDebug() << "Debug ShiftState" << QString::number(ShiftState);
    txt_info += "GetShiftState: " + QString::number(retval) + "\r";
    txt_info += "ShiftState: " + QString::number(ShiftState) + "\rShiftTime: " + QString::number(ShiftTime) + "\r";


    char tableno[100];
    char placeno[100];
    char accountno[100];
    char freefield[300];

    int8_t doctype = 0;
    if(doctype > 0) { doctype += 1; }
    int8_t copies = 1;
    int8_t taxation = 0;

    tempqstr = "";
    std::strncpy(tableno, cpcurrent->fromUnicode(tempqstr).constData(), 90);

    tempqstr = "";
    std::strncpy(placeno, cpcurrent->fromUnicode(tempqstr).constData(), 90);

    tempqstr = "";
    std::strncpy(accountno, cpcurrent->fromUnicode(tempqstr).constData(), 90);

    tempqstr = "";
    std::strncpy(freefield, cpcurrent->fromUnicode(tempqstr).constData(), 280);

    retval = StartReceiptPlus(doctype, copies, taxation, tableno, placeno, accountno, freefield);
    txt_info += "StartReceiptPlus: " + QString::number(retval) + "\r";

    char warename[100];
    char warecode[100];
    char measure[100];
    char secid[100];
    //char freefield[300];
    char price[100];
    char count[100];

    tempqstr = "Билет.";
    std::strncpy(warename, cpcurrent->fromUnicode(tempqstr).constData(), 90);

    tempqstr = "";
    std::strncpy(warecode, cpcurrent->fromUnicode(tempqstr).constData(), 90);

    tempqstr = "шт.";
    std::strncpy(measure, cpcurrent->fromUnicode(tempqstr).constData(), 90);

    tempqstr = "";
    std::strncpy(secid, cpcurrent->fromUnicode(tempqstr).constData(), 90);

    tempqstr = "";
    std::strncpy(freefield, cpcurrent->fromUnicode(tempqstr).constData(), 280);

    tempqstr = "124.00";
    std::strncpy(price, cpcurrent->fromUnicode(tempqstr).constData(), 90);

    tempqstr = "1";
    std::strncpy(count, cpcurrent->fromUnicode(tempqstr).constData(), 90);

    tempqstr = "";
    uint16_t waretype = tempqstr.toInt();

    tempqstr = "";
    waretype += (tempqstr.toInt() << 8);

    int8_t itemtype = 1;

    int8_t payitemtype = 1;

    int8_t agenttype = 0;

    retval = ItemReceiptExx20(warename, warecode, measure, secid, freefield,
                                      price, count, waretype, itemtype, payitemtype,
                                      agenttype);
    txt_info += "ItemReceiptExx20: " + QString::number(retval) + "\r";



    //char freefield[300];

    tempqstr = "Дубки -> 105 км.";
    std::strncpy(freefield, cpcurrent->fromUnicode(tempqstr).constData(), 280);

    retval = TotalReceiptPlus(freefield);

    txt_info += "TotalReceiptPlus: " + QString::number(retval) + "\r";


    char tendersum[100];
    char cardname[100];
    //char freefield[300];

    tempqstr = "0";
    int8_t paytype = tempqstr.toInt();

    tempqstr = "150";
    std::strncpy(tendersum, cpcurrent->fromUnicode(tempqstr).constData(), 90);

    tempqstr = "";
    std::strncpy(cardname, cpcurrent->fromUnicode(tempqstr).constData(), 90);

    tempqstr = "";
    std::strncpy(freefield, cpcurrent->fromUnicode(tempqstr).constData(), 280);

    retval = TenderReceiptEx(paytype, tendersum, cardname, freefield);
    txt_info += "TotalReceiptPlus: " + QString::number(retval) + "\r";


    QThread::sleep(1);

    retval = CloseReceipt();
    txt_info += "CloseReceipt: " + QString::number(retval) + "\r";


    QThread::sleep(1);


    retval = ShiftClosePlus(oprname, emptystr, emptystr, isprint);
    txt_info += "ShiftClosePlus: " + QString::number(retval) + "\r";

    LogEnable(1);
    QThread::sleep(1);

    retval = GetShiftState(&ShiftState, &ShiftTime);

    //outaddline(utf8->toUnicode("Штрих-код: "), tempstr);
//    qDebug() << "Debug ShiftTime" << QString::number(ShiftTime);
//    qDebug() << "Debug ShiftState" << QString::number(ShiftState);
    txt_info += "GetShiftState: " + QString::number(retval) + "\r";
    txt_info += "ShiftState: " + QString::number(ShiftState) + "\rShiftTime: " + QString::number(ShiftTime) + "\r";

    retval = FNSendMessageOFD();
    txt_info += "FNSendMessageOFD: " + QString::number(retval) + "\r";

    QThread::sleep(1);

    retval = FNSendOFD();
    txt_info += "FNSendOFD: " + QString::number(retval) + "\r";

    retval = CloseDLL();
    txt_info += "CloseDLL: " + QString::number(retval) + "\r";

    showInfo(txt_info);
    waitHide();
}

void MainWindow::on_cmd_2dCode_clicked()
{
    waitShow();





    QString txt = "";

    txt += shell_command("echo 1 > /sys/class/leds/barscan_power_ena/brightness");
    txt += shell_command("echo 1 > /sys/class/leds/barscan_reset/brightness");
    txt += shell_command("echo 1 > /sys/class/leds/barscan_trigger/brightness");

    QSerialPort serial;
    serial.setPortName("/dev/ttymxc2");
    serial.setBaudRate(QSerialPort::Baud9600);
    serial.setDataBits(QSerialPort::Data8);
    // serial.setParity(QSerialPort::NoParity);
    // serial.setStopBits(QSerialPort::OneStop);
    // serial.setFlowControl(QSerialPort::NoFlowControl);


    if (serial.open(QIODevice::ReadWrite))
    {
        //txt += "Port Open";
        //Connected
        //txt+= serial.readAll();
    }
    else
    {
        txt += "Port Error";
        //Open error
    }

    for(int i =0; i < 10; i ++)
    {
        QThread::sleep(1);
        qApp->processEvents();

        txt = serial.readAll();

        if(txt != "")
        {

            break;
        }
    }

    serial.close();

    txt += shell_command("echo 0 > /sys/class/leds/barscan_power_ena/brightness");
    txt += shell_command("echo 0 > /sys/class/leds/barscan_reset/brightness");
    txt += shell_command("echo 0 > /sys/class/leds/barscan_trigger/brightness");

    ui->txtText->setText(txt);

    waitHide();
}

void MainWindow::on_cmd_Transitek_clicked()
{
    this->setEnabled(false);
    QString txt = "";
    ui->txtText->setText(txt);

    QString next = "";

    QPixmap m_logo_pic;
    //m_logo_pic.load("/home/vadim/projects/git_projects/mk100t/bank.png");
    m_logo_pic.load("/root/mk100/mk100t/bank.png");
    ui->lblPIC->setPixmap(m_logo_pic);
    ui->lblPIC->setVisible(true);

    waitShow("ПОДНЕСИТЕ КАРТУ");

    this->repaint();
    this->update();

    qApp->processEvents();

    //QThread::sleep(5);

    //txt += shell_command("echo 1 > /sys/class/leds/ena_bskpwr/brightness && export PYTHONPATH=/root/ir_examples_1.0.275633b && cd /root/ir_examples_1.0.275633b && python3 irc.py -c /dev/ttymxc3 perform_txn");
    //txt += shell_command("PYTHONPATH=/home/vadim/projects/Transitek/ir_examples_1.0.275633b && cd /home/vadim/projects/Transitek/ir_examples_1.0.275633b && source ./setenv.sh && ./irc.py --demo");

    QString sppedRS242 = ui->cmb_fonarik->currentText();
    QString runCommand = "echo 1 > /sys/class/leds/ena_bskpwr/brightness && export PYTHONPATH=/root/ir_examples_1.0.275633b && cd /root/ir_examples_1.0.275633b &&python3 irc.py -c /dev/ttymxc3 --demo -b" + sppedRS242 ;

    shell_command("/root/mk100/mk100t/sleepTransitek.sh &");
    txt += shell_command(runCommand.toLatin1());


    for(int i = 0; i < 3; i ++)
    {
        int comPos = txt.indexOf("status");

        int comPosN = txt.indexOf("NON_EMV_CARD");

        int comEndPos  = 0;
        if(comPosN - comPos < 10)
        {
            comEndPos = comPos + 150;
        }
        else
        {
            comEndPos = txt.indexOf("emv_tags { number: 5F24", comPos);
        }

        QString subStr = txt.mid(comPos, comEndPos - comPos);
        next += subStr + "\r\n ================= \r\n";
        txt = txt.mid(comPos + 200);
    }

    //QString subStr = txt.mid(-1, -1);
    //txt = subStr
    txt += shell_command("echo 0 > /sys/class/leds/ena_bskpwr/brightness");
    ui->cmd_Transitek->setEnabled(false);
    ui->checkBox_transitek->setChecked(false);

    ui->txtText->setText(next);

    ui->lblPIC->setVisible(false);

    qApp->processEvents();
    this->setEnabled(true);

    qApp->processEvents();

    waitHide();

    waitHide();
}

void MainWindow::on_cmd_sound_clicked()
{

    qApp->processEvents();
    this->setEnabled(false);
    qApp->processEvents();
    QString txt = "";
    ui->txtText->setText(txt);
    qApp->processEvents();

    // radio online
    // mplayer -ao alsa:device=hw=0.0 http://us5.internet-radio.com:8267/stream
    // mplayer -ao alsa:device=hw=0.0 http://transamerica.crossradio.com.br:9100/stream/
    // mplayer -ao alsa:device=hw=0.0 http://uk2.internet-radio.com:8024/stream

    // /root/SoundTest4.wav
    //txt += shell_command("alsactl -f /root/asound.state restore && aplay -D hw:0,0 /root/unfog.wav");

    txt += shell_command("alsactl -f /root/asound.state restore && aplay -D hw:0,0 /root/SoundTest4.wav");

    showInfo(txt);

    //showInfo(txt);

    qApp->processEvents();
    this->setEnabled(true);
    qApp->processEvents();
}

/*
# record from mic
arecord -D hw:0,0 -f S16_LE -r 44100 -c 2 /tmp/1.wav
# playe recorded
aplay -D hw:0,0 /tmp/1.wav
*/

/*
gnome-open http://ubuntovod.ru
sudo apt-get install links
links ubuntovod.ru
*/

/*
sudo lshw
*/

void MainWindow::on_cmd_internet_clicked()
{
    waitShow();
    QString txt = "";
    ui->txtText->setFontPointSize(5);

    txt += shell_command("curl wttr.in/moscow");

    showInfo(txt);

    waitHide();
}

void MainWindow::on_cmd_backMain_clicked()
{
    system("pkill xgps");

    ui->tab_Main->setCurrentIndex(0);
    ui->txtText->setFontPointSize(30);
    ui->cmd_UpdateAcceler->setVisible(false);
}

void MainWindow::on_cmd_exit_clicked()
{
    //    QMessageBox msgBox;

    //    msgBox.setStyleSheet("font: 50pt \"Ubuntu\";");
    //    msgBox.setText("Вы нажали кнопку выходы.");
    //    //msgBox.move(0,0);
    //    //msgBox.setWindowFlag(Qt::WindowStaysOnTopHint);
    //    //msgBox.showFullScreen();
    //    msgBox.setModal(true);
    //    msgBox.setGeometry(100, 100, 100, 100);
    //    //msgBox.resize(720, 1280);
    //    //msgBox.move(0,0);
    //    msgBox.exec();


    //    QMessageBox msgBox;
    //    //msgBox.setStyleSheet("font: 50pt \"Ubuntu\";");
    //    msgBox.setIcon(QMessageBox::Information);
    //    msgBox.resize(250, 200);
    //    msgBox.setText("\n \n \n \n \n \n \n \n \n\ n\ \n \n    ВЫХОД");
    //    msgBox.setInformativeText("ВЫХОД");
    //    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    //    msgBox.setDefaultButton(QMessageBox::No);
    //    msgBox.setEscapeButton(QMessageBox::No);
    //    msgBox.setWindowFlag(Qt::FramelessWindowHint,true);
    //    msgBox.setStyleSheet("font: 50pt \"Ubuntu\"; QMessageBox{font: 50pt \"Ubuntu\"; border: 10px solid black; background-color:white}");
    //    msgBox.move(0,0);

    //    if (msgBox.exec() == QMessageBox::Yes)
    //    {

    //    }

    //    return;

    waitShow();

//////////////////////////////////////////////////////
//    //char oprname[100];
//    int8_t isprint = 0;
//    char emptystr[3];
//    emptystr[0] = 0;
//    char addreq[100] ; //=  "Доп. информация";

//    int32_t retval;
//    QString txt_info = "";
//    char oprname[30] ; //s= "Иванов И. И.";
//    char pswd[] = "AERF";

//    char devname1[] = "/dev/ttySC0";
//    char devname2[] = "/dev/rfcomm0";

//    char baudrate[] = "115200";
//    int32_t flagoem = 1;

//    if(flagoem == 0) {
//        cpcurrent = cp866;
//    }
//    else {
//        cpcurrent = cp1251;
//    }

//    tempqstr = "Иванов И. И.";
//    std::strncpy(oprname, cpcurrent->fromUnicode(tempqstr).constData(), 30);

//    tempqstr = "Доп. информация";
//    std::strncpy(addreq, cpcurrent->fromUnicode(tempqstr).constData(), 30);

//    qApp->processEvents();

//    if(ui->cmb_kkt->currentIndex() == 0)
//    {
//        retval = OpenDLLPlus(oprname, pswd, devname1, baudrate, flagoem);
//    }
//    else
//    {
//        retval = OpenDLLPlus(oprname, pswd, devname2, baudrate, flagoem);
//    }

//    txt_info += "OpenDLLPlus: " + QString::number(retval) + "\r";


//    retval = LowPowerState();


//    retval = CheckHealth();
//    QThread::sleep(1);
//    retval = CheckHealth();


//    int32_t ShiftState = 0;
//    int32_t ShiftTime = 0;

//    retval = GetShiftState(&ShiftState, &ShiftTime);

//    //outaddline(utf8->toUnicode("Штрих-код: "), tempstr);

//    qDebug() << "Debug GetShiftState" << QString::number(retval);
//    qDebug() << "Debug ShiftTime" << QString::number(ShiftTime);
//    qDebug() << "Debug ShiftState" << QString::number(ShiftState);

//    retval = BarcodeManager(1, 0);
//    qDebug() << "Debug GetShiftState 1" << QString::number(retval);
//    retval = BarcodeManager(2, 0);
//    qDebug() << "Debug GetShiftState 2" << QString::number(retval);
//    retval = BarcodeManager(3, 10000);
//    if(retval == 0)
//    {
//        char datestr[1024];
//        retval = GetFldStr(5,datestr);
//        QString dateString = QString::fromLatin1(datestr, retval);

//        qDebug() << "Debug GetFldStr 5" << dateString;
//    }
//    qDebug() << "Debug GetShiftState 3" << QString::number(retval);
//    retval = BarcodeManager(4, 0);
//    qDebug() << "Debug GetShiftState 4" << QString::number(retval);

//    retval = LowPowerState();

//    retval = CheckHealth();
//    QThread::sleep(1);
//    retval = CheckHealth();

//    retval = CloseDLL();
//    txt_info += "CloseDLL: " + QString::number(retval) + "\r";
//////////////////////////////////////////////////////


    //pr_mod_term.start("/bin/sh", QStringList() << "-c" << "/mnt/store/install_os.sh");
    //QThread::sleep(30);

    shell_command("echo 1 > /sys/class/leds/gsm_pwr_key/brightness # pwrbutton press");
    QThread::sleep(1);
    shell_command("echo 0 > /sys/class/leds/gsm_pwr_key/brightness # pwrbutton not pressed");

    shell_command("echo 0 > /sys/class/leds/ena_bskpwr/brightness");

    shell_command("echo 0 > /sys/class/leds/ena_bskpwr/brightness");

    shell_command("echo 0 > /sys/class/leds/charger_board_kkt_power/brightness");

    shell_command("echo 0 > /sys/class/leds/charger_board_kkt_power/brightness");

    shell_command("pkill monitor-sensor");

    this->close();
    qApp->exit(0);
}

void MainWindow::on_cmd_sysInfo_clicked()
{
    waitShow();
    QString txt = "";
    ui->txtText->setFontPointSize(22);

    txt += QDir::currentPath();
    txt += "\n\r";

    txt += shell_command("nmcli -t -f active,ssid dev wifi | egrep '^yes' | cut -d\\' -f2");
    txt += "\n\r";

//    txt += shell_command("uname -a");
//    txt += "\n\r";

    txt += shell_command("uname -a");
    txt += "\n\r";

    //txt += shell_command("ip addr show wlan0 | grep ""inet """);

    txt += shell_command("ip addr");
    txt += "\n\r";

    txt += shell_command("lshw");
    txt += "\n\r";

    shell_command("echo 0 > /sys/class/leds/ena_bskpwr/brightness");

    showInfo(txt);

    waitHide();
}

void MainWindow::on_cmd_asseler_clicked()
{
    waitShow();
    QString txt = "";

    for(int i = 0; i < 1; i++)
    {
        //time
        txt = shell_command("gdbus call --system --dest net.hadess.SensorProxy --object-path /net/hadess/SensorProxy --method org.freedesktop.DBus.Properties.Get net.hadess.SensorProxy AccelerometerOrientation");
        showInfo(txt);

        qApp->processEvents();
        ui->tab_Main->setCurrentIndex(1);
    }

//    if(txt .trimmed() == "(<'right-up'>,)")
//    {
//        txt = "экран от себя";
//    }

//    if(txt .trimmed() == "(<'normal'>,)")
//    {
//        txt = "экраном вверх";
//    }

//    if(txt .trimmed() == "(<'left-up'>,)")
//    {
//        txt = "экран к себе";
//    }

//    if(txt .trimmed() == "(<'bottom-up'>,)")
//    {
//        txt = "экраном вниз";
//    }

    if(txt .trimmed() == "(<'right-up'>,)")
    {
        //txt = "экраном от себя";
        txt = "экраном к себе";
    }

    if(txt .trimmed() == "(<'normal'>,)")
    {
        txt = "экраном вверх";
    }

    if(txt .trimmed() == "(<'left-up'>,)")
    {
        //txt = "экраном к себе";
        txt = "экраном от себя";
    }

    if(txt .trimmed() == "(<'bottom-up'>,)")
    {
        txt = "экраном вниз";
    }

    showInfo(txt);
    waitHide();

    ui->cmd_UpdateAcceler->setVisible(true);
}

void MainWindow::on_cmd_bluetoth_clicked()
{
    waitShow();
    QString txt = "";

    txt += shell_command("hcitool -i hci0 scan");

    showInfo(txt);

    waitHide();
}

void MainWindow::on_cmd_record_clicked()
{
    waitShow("ГОВОРИТЕ В МИКРОФОН\r\n 5 СЕКУНД");
    QString txt = "";

    txt += shell_command("arecord -D hw:0,0 -f S16_LE -r 44100 -c 2 -d 5 /tmp/1.wav");
    //txt += shell_command("vi", 10000);

    waitShow("ПРОСЛУШАЙТЕ ЗАПИСЬ");

    txt += shell_command("aplay -D hw:0,0 /tmp/1.wav");

    showInfo(txt);

    waitHide();
}

// pkill -f firefox
// xdotool search --sync --onlyvisible --class "gnome-terminal" windowmove 100 100


void MainWindow::on_cmd_gps_clicked()
{

    // DISPLAY=:0 weston-terminal -f --shell=/usr/bin/cgps

    // DISPLAY=:0 weston-terminal -f --shell=/usr/bin/cgps --font-size=16

    //fuser /dev/ttyUSB1

    //systemctl stop gpsd.socket
    //systemctl start gpsd.socket
    //systemctl status gpsd.socket


    waitShow();
    QString txt = "";

    //echo $?
    // cgps // client terminal

//    systemctl stop gpsd.socket
//    systemctl start gpsd.socket

    //txt += shell_command("gpspipe -w -n 10");

    shell_command("systemctl restart gpsd.socket");
    QThread::sleep(1);

    //system("DISPLAY=:0 LANG=en_US.UTF-8 xgps &");

    system("DISPLAY=:0 weston-terminal -f --shell=/usr/bin/cgps --font-size=16 &");

    QThread::sleep(10);

    //txt += shell_command("DISPLAY=:0 LANG=en_US.UTF-8 xgps &");

    //QThread::sleep(1);

    //txt += shell_command("DISPLAY=:0 xdotool search --sync --onlyvisible --class ""xgps"" windowmove 0 0");

    ui->txtText->setFontPointSize(16);

    for(int i = 0; i < 10; i++)
    {
        showInfo(gps_string);
        ui->tab_Main->setCurrentIndex(1);
        qApp->processEvents();
        QThread::sleep(1);
        qApp->processEvents();
    }

    //txt += shell_command("pkill -f xgps");

    txt += shell_command("pkill -f weston-terminal");


    ui->txtText->setFontPointSize(30);
    txt += "ТЕСТ ОКОНЧЕН";

//    qApp->processEvents();
//    this->setEnabled(true);
//    ui->tab_Main->setCurrentIndex(0);
//    qApp->processEvents();

    showInfo(txt);
    waitHide();
}

void MainWindow::on_cmb_light_activated(const QString &arg1)
{

}

void MainWindow::on_cmb_light_activated(int index)
{
    //waitShow();
    QString txt = "";

    QString backlight_num = QString::number((index +1) * 10);
    QString backlight = "echo " + backlight_num + " > /sys/class/backlight/backlight-dsi/brightness";

    txt += shell_command(backlight.toLatin1());

    //showInfo(txt);

    //waitHide();
}

void MainWindow::on_cmd_wifi_clicked()
{
    waitShow();
    QString txt = "";
    ui->txtText->setFontPointSize(5);

    txt += shell_command("curl wttr.in/moscow");

    showInfo(txt);

    waitHide();
}

void MainWindow::on_cmd_modem_clicked()
{
    waitShow();
    QString txt = "";
    ui->txtText->setFontPointSize(25);

    //QString command = " gnome-terminal  -x ="" \'curl wttr.in/moscow; sleep 10; exit; $SHELL\'"" ";

    //# info internet
    // https://ipinfo.io/
    // curl ipinfo.io/hostname

    // curl ipinfo.io/org
    // "ping -c 10 -v  ya.ru"
    //txt += shell_command("curl wttr.in/moscow"); 87.250.250.242
    //txt += shell_command("ping -c 4 -v  ya.ru");
    txt += shell_command("ping -c 4 -v 87.250.250.242");
    //txt += shell_command(command.toLatin1());

    //pr_mod_term.start("/bin/sh", QStringList() << "-c" << command);

    txt += shell_command("curl ipinfo.io/hostname", -1, 0x01);
    txt += shell_command("curl ipinfo.io/loc", -1, 0x01);

//    txt += shell_command("curl ipinfo.io/hostname") + "\n";
//    txt += shell_command("curl ipinfo.io/city") + "\n";
//    txt += shell_command("curl ipinfo.io/country");
//    txt += shell_command("curl ipinfo.io/loc") + "\n";
//    txt += shell_command("curl ipinfo.io/postal") + "\n";

    showInfo(txt);

    waitHide();
}

void MainWindow::on_cmd_scanKKT_clicked()
{
    waitShow();


    ////////////////////////////////////////////////////
        //char oprname[100];
        int8_t isprint = 0;
        char emptystr[3];
        emptystr[0] = 0;
        char addreq[100] ; //=  "Доп. информация";

        int32_t retval;
        QString txt_info = "";
        char oprname[30] ; //s= "Иванов И. И.";
        char pswd[] = "AERF";

        char devname1[] = "/dev/ttySC0";
        char devname2[] = "/dev/rfcomm0";
        char devname3[] = "/dev/ttymxc2";

        char baudrate[] = "115200";
        int32_t flagoem = 1;

        if(flagoem == 0) {
            cpcurrent = cp866;
        }
        else {
            cpcurrent = cp1251;
        }

        tempqstr = "Иванов И. И.";
        std::strncpy(oprname, cpcurrent->fromUnicode(tempqstr).constData(), 30);

        tempqstr = "Доп. информация";
        std::strncpy(addreq, cpcurrent->fromUnicode(tempqstr).constData(), 30);

        qApp->processEvents();

        if(ui->cmb_kkt->currentIndex() == 0)
        {
        retval = OpenDLLPlus(oprname, pswd, devname1, baudrate, flagoem);
        }
        else if(ui->cmb_kkt->currentIndex() == 2)
        {
        retval = OpenDLLPlus(oprname, pswd, devname3, baudrate, flagoem);
        }
        else
        {
        retval = OpenDLLPlus(oprname, pswd, devname2, baudrate, flagoem);
        }

//        if(ui->cmb_kkt->currentIndex() == 0)
//        {
//            retval = OpenDLLPlus(oprname, pswd, devname1, baudrate, flagoem);
//        }
//        else
//        {
//            retval = OpenDLLPlus(oprname, pswd, devname2, baudrate, flagoem);
//        }

        txt_info += "OpenDLLPlus: " + QString::number(retval) + "\r";


//        retval = LowPowerState();


//        retval = CheckHealth();
//        QThread::sleep(1);
//        retval = CheckHealth();


//        int32_t ShiftState = 0;
//        int32_t ShiftTime = 0;

//        retval = GetShiftState(&ShiftState, &ShiftTime);

//        //outaddline(utf8->toUnicode("Штрих-код: "), tempstr);

//        qDebug() << "Debug GetShiftState" << QString::number(retval);
//        qDebug() << "Debug ShiftTime" << QString::number(ShiftTime);
//        qDebug() << "Debug ShiftState" << QString::number(ShiftState);

        retval = BarcodeManager(1, 0);
        qDebug() << "Debug GetShiftState 1" << QString::number(retval);
        txt_info += "BarcodeManager 1 " + QString::number(retval) +  "\r";
        retval = BarcodeManager(2, 0);
        qDebug() << "Debug GetShiftState 2 " << QString::number(retval);
        txt_info += "BarcodeManager 2 " + QString::number(retval) +  "\r";
        retval = BarcodeManager(3, 10000);
        txt_info += "BarcodeManager 3" + QString::number(retval) +  "\r";
        if(retval == 0)
        {
            char datestr[1024];
            for(int i = 0; i < 1024; i++)
            {
                datestr[i] = 0x00;
            }

            GetFldStr(5, datestr);

            QString dateString = QString::fromUtf8(datestr);

            qDebug() << "Debug GetFldStr 5" << dateString;
            txt_info += "BarcodeManager 3 data: " + dateString +  "\r";
        }
        qDebug() << "Debug GetShiftState 3" << QString::number(retval);
        retval = BarcodeManager(4, 0);
        txt_info += "BarcodeManager 4" + QString::number(retval);
        qDebug() << "Debug GetShiftState 4" << QString::number(retval);

//        retval = LowPowerState();

//        retval = CheckHealth();
//        QThread::sleep(1);
//        retval = CheckHealth();

        retval = CloseDLL();
        txt_info += "CloseDLL: " + QString::number(retval) + "\r";

        ui->txtText->setText(txt_info);

    ////////////////////////////////////////////////////


//    QString portName = "";

////    char devname1[] = "/dev/ttySC0";
////    char devname2[] = "/dev/rfcomm0";

//    if(ui->cmb_kkt->currentIndex() == 0)
//    {
//        portName = "/dev/ttySC0";
//    }
//    else
//    {
//        portName = "/dev/rfcomm0";
//    }

//    QString txt = "";
//    QSerialPort serial;
//    serial.setPortName(portName);
//    serial.setBaudRate(QSerialPort::Baud115200);
//    serial.setDataBits(QSerialPort::Data8);

//    char cmd_on[] = {0x02, 0x41, 0x45, 0x52, 0x46, 0x33, 0x42, 0x30, 0x1c, 0x30, 0x31, 0x1c, 0x31, 0x30, 0x32, 0x37, 0x1c, 0x03, 0x34, 0x37, 0x30, 0x33 };

//    char readCODE[128];

//    if (serial.open(QIODevice::ReadWrite))
//    {
//        //txt += "Port Open";
//        //serial.flush();
//        qApp->processEvents();
//        int resI = serial.readLine(readCODE, 128);
//        qApp->processEvents();
//        serial.reset();
//        resI = serial.readLine(readCODE, 128);
//        qApp->processEvents();
//        serial.write(cmd_on, strlen(cmd_on));
//        qApp->processEvents();
//    }
//    else
//    {
//        txt += "Port Error";

//    }

//    for(int i =0; i < 13; i ++)
//    {
//        QThread::sleep(1);
//        qApp->processEvents();
//        int resI = serial.readLine(readCODE, 128);

//        if(resI > 0)
//        {
//            txt = readCODE;
//            if(txt != "")
//            {
//                QStringList rf = txt.split('\u001C');

//                addLog("KKT SCAN" + txt);
//                qDebug() << txt;

//                for(int i = 0; i < rf.length(); i ++)
//                {
//                    addLog("KKT SCAN" + rf[i]);
//                    qDebug() << rf[i];
//                }

//                if(rf.length() > 0)
//                {
//                    if(rf[3] == "0000")
//                    {
//                        QString str1 = rf[5];
//                        qDebug() << " rf[5]:" <<  rf[5];
//                        QByteArray textF = QByteArray::fromHex(rf[5].toLatin1());
//                        QString str2 = textF.data();            // returns "Teleshopping"
//                        qDebug() << textF;
//                        //QByteArray hex =  QByteArray::fromHex(str1.toLatin1());
//                        //QString str2 = QString::fromUtf16((char16_t*)hex.data());
//                        txt = str2;
//                    }
//                    else
//                    {
//                        txt = "";
//                    }
//                }
//                else
//                {
//                    txt = "";
//                }

//                break;
//            }
//        }
//    }

//    serial.close();

//    ui->txtText->setText(txt);

    waitHide();
}

void MainWindow::on_cmb_kkt_activated(int index)
{
    if(index == 0)
    {
        ui->cmb_kkt->setStyleSheet("background-color: rgb(138, 226, 52)");
    }
    else
    {
        ui->cmb_kkt->setStyleSheet("background-color: rgb(52, 101, 164)");
    }
}

void MainWindow::on_cmb_manager_internet_activated(int index)
{
    //nmcli radio wifi on
    //nmcli radio wifi off

    //systemctl stop bluetooth.service
    //systemctl disable bluetooth.service

    //waitShow();
    QString txt = "";

    if(index == 0)
    {
        txt += shell_command("nmcli radio wifi on");
    }
    else
    {
        txt += shell_command("nmcli radio wifi off");
    }

    //showInfo(txt);
    //waitHide();
}

void MainWindow::on_cmb_manager_internet_activated(const QString &arg1)
{

}

void MainWindow::on_cmd_battery_clicked()
{
    waitShow();


    //#
    //+ a) подсоединена зарядка или нет 	 # cat /sys/class/power_supply/bq25700-charger/online (0,1)
    //+ b) заряжается батарея или нет	 # cat /sys/class/power_supply/bq25700-charger/status (Discharging,Charging)
    //+ c) уровень заряженности батареи в %   # cat /sys/class/power_supply/max1726x_battery/capacity
    //cat /sys/class/power_supply/max1726x_battery/voltage_now

    //    cat /sys/class/power_supply/max1726x_battery/capacity   # уровень заряда батареи, % (0-100)
    //    cat /sys/class/power_supply/max1726x_battery/charge_now   # если 0 - не заряжается

    // cat /sys/class/power_supply/bq25700-charger/constant_charge_current

    // nmtui

    //isCharge = shell_command("cat /sys/class/power_supply/max1726x_battery/charge_now");
    //isCharge = shell_command("cat /sys/class/power_supply/bq25700-charger/constant_charge_current");

    QString txt = "";
    QString capacity = "";
    QString ver = "";
    QString online = "";
    QString status = "";
    QString voltage_now = "";

    if(verPowerBoard == 1)
    {
        capacity = shell_command("cat /sys/class/power_supply/max1726x_battery/capacity");
        status = shell_command("cat /sys/class/power_supply/bq25700-charger/status");
        ver = shell_command("cat /sys/class/i2c-dev/i2c-2/device/2-0040/charger_board_ver");
        online = shell_command("cat /sys/class/power_supply/bq25700-charger/online");
        voltage_now = shell_command("cat /sys/class/power_supply/max1726x_battery/voltage_now");
    }
    else
    {
        capacity = shell_command("cat /sys/class/power_supply/bq27520-0/capacity");
        status = shell_command("cat /sys/class/power_supply/bq27520-0/status");
        ver = shell_command("cat /sys/class/i2c-dev/i2c-2/device/2-0010/charger_board_ver");
        online = shell_command("cat /sys/class/power_supply/mp2669a_usb/online"); // cat /sys/class/power_supply/bq25700-charger/online
        voltage_now = shell_command("cat /sys/class/power_supply/bq27520-0/voltage_now");
    }



    if(status.trimmed() == "Charging")
    {
        status = "Заряжается";
    }
    else
    {
        status = "Не заряжается";
    }

    if(online.trimmed() == "1")
    {
        online = "Подключен";
    }
    else
    {
        online = "Не подключен";
    }

    double volt_int = voltage_now.toInt();

    txt += "\nУровень заряда(%): " + capacity + " \n";
    txt += "\nСтатус зарядки: " + status + "\n\n";
    txt += "\nСтатус кабеля: " + online + "\n\n";
    txt +=  "\nВерсия: " + ver + "\n";
    txt +=  "\nНапряжение(V): " + QString::number(volt_int/1000000) + "\n";


//    if(isCharge.trimmed() == "0")
//    {
//        txt += "Нет зарядки";
//    }
//    else
//    {
//        txt += "Зарядка";
//    }




//    txt += shell_command("/root/chg.sh");
//    if(txt.trimmed() == "not charge")
//    {
//        txt += " нет зарядки";
//    }
//    if(txt.trimmed()  == "charge")
//    {
//        txt += " зарядка";
//    }

    showInfo(txt);

    waitHide();
}

void MainWindow::on_cmd_lighten_clicked()
{
    waitShow();
    QString txt = "";

    txt += shell_command("cat /sys/bus/iio/devices/iio:device0/in_illuminance_input");

    showInfo(txt.trimmed().replace("/r/n","") + " лк.");

    waitHide();
}

void MainWindow::on_cmd_UpdateAcceler_clicked()
{
    QString txt = shell_command("gdbus call --system --dest net.hadess.SensorProxy --object-path /net/hadess/SensorProxy --method org.freedesktop.DBus.Properties.Get net.hadess.SensorProxy AccelerometerOrientation");

//    (<'right-up'>,)  экран от себя
//    (<'normal'>,)    экраном вверх
//    (<'left-up'>,)   экран к себе
//    (<'bottom-up'>,) экраном вниз

//    if(txt .trimmed() == "(<'right-up'>,)")
//    {
//        //txt = "экран от себя";
//        txt = "экран к себе";
//    }

//    if(txt .trimmed() == "(<'normal'>,)")
//    {
//        txt = "экраном вверх";
//    }

//    if(txt .trimmed() == "(<'left-up'>,)")
//    {
//        //txt = "экран к себе";
//        txt = "экран от себя";
//    }

//    if(txt .trimmed() == "(<'bottom-up'>,)")
//    {
//        txt = "экраном вниз";
//    }


    if(txt .trimmed() == "(<'right-up'>,)")
    {
        //txt = "экран от себя";
        txt = "экран к себе";
    }

    if(txt .trimmed() == "(<'normal'>,)")
    {
        txt = "экраном вверх";
    }

    if(txt .trimmed() == "(<'left-up'>,)")
    {
        //txt = "экран к себе";
        txt = "экран от себя";
    }

    if(txt .trimmed() == "(<'bottom-up'>,)")
    {
        txt = "экраном вниз";
    }

    showInfo(txt);
    qApp->processEvents();
    //QThread::sleep(1);
    ui->tab_Main->setCurrentIndex(1);
}

void MainWindow::on_cmd_fullLight_clicked()
{
}

void MainWindow::on_cmb_fonarik_activated(int index)
{
//    ////////////////////////////////////////////
//    QString txt = "";

//    if(index == 0)
//    {
//        txt = shell_command("stty -F /dev/ttySC1 115200 raw; echo -n -e 'ledoff\r' > /dev/ttySC1; echo -n -e 'ledoff\r' > /dev/ttySC1; echo -n -e 'ledoff\r' > /dev/ttySC1");
//    }
//    else
//    {
//        txt = shell_command("stty -F /dev/ttySC1 115200 raw; echo -n -e 'ledon\r' > /dev/ttySC1; echo -n -e 'ledon\r' > /dev/ttySC1; echo -n -e 'ledon\r' > /dev/ttySC1");
//    }
//    ////////////////////////////////////////////
}

void MainWindow::on_cmd_exit_2_clicked()
{

        ui->tab_Main->setCurrentIndex(PageWindows::pageExit);

        /*

        QMessageBox msgBox;
        //msgBox.setStyleSheet("font: 50pt \"Ubuntu\";");
        msgBox.setIcon(QMessageBox::Information);
        msgBox.resize(250, 200);
        msgBox.setText("\n \n \n \n \n \n \n \n \n \nВЫКЛЮЧИТЬ ТЕРМИНАЛ?");
        msgBox.setInformativeText("             \n            ");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::No);
        msgBox.setEscapeButton(QMessageBox::No);





        msgBox.setButtonText(QMessageBox::Yes, trUtf8("ДА"));
        msgBox.setButtonText(QMessageBox::No, trUtf8("НЕТ"));

        //msgBox.setWindowFlag(Qt::FramelessWindowHint  ,true);
        //msgBox.setWindowFlags(Qt::WindowStaysOnTopHint);
        msgBox.setWindowModality(Qt::NonModal);
        msgBox.setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog | Qt::WindowStaysOnTopHint);
//        msgBox.setWindowFlags(windowFlags()| Qt::FramelessWindowHint);
//        msgBox.setWindowFlags(windowFlags()|Qt::WindowStaysOnTopHint);
        msgBox.setStyleSheet("font: 50pt \"Ubuntu\"; QMessageBox{font: 70pt \"Ubuntu\"; border: 10px solid black; background-color:white}");
        msgBox.move(0,0);
        msgBox.move(0,0);
        //msgBox.showFullScreen();
        //msgBox.showMaximized();

        if (msgBox.exec() != QMessageBox::Yes)
        {
            return;
        }

        shell_command("/root/poweroff.sh");

        shell_command("echo 1 > /sys/class/leds/gsm_pwr_key/brightness # pwrbutton press");
        QThread::sleep(1);
        shell_command("echo 0 > /sys/class/leds/gsm_pwr_key/brightness # pwrbutton not pressed");

        //shell_command("echo 1 > /sys/class/leds/ena_bskpwr/brightness");

        shell_command("echo 0 > /sys/class/leds/ena_bskpwr/brightness");

        shell_command("echo 0 > /sys/class/leds/ena_bskpwr/brightness");

        shell_command("echo 1 > /sys/class/leds/charger_board_power_off/brightness");

        shell_command("poweroff");

        this->close();
        qApp->exit(0);

        */
}

void MainWindow::on_cmd_power_clicked()
{
    ui->tab_Main->setCurrentIndex(3);
}

void MainWindow::on_cmd_power_back_clicked()
{
    ui->tab_Main->setCurrentIndex(0);
}

void MainWindow::usbHUBEnable(bool enable)
{
    //    # echo -n "0000:00:14.0" | sudo tee /sys/bus/pci/drivers/xhci_hcd/unbind
    //    # echo -n "0000:00:14.0" | sudo tee /sys/bus/pci/drivers/xhci_hcd/bind

    // /sys/bus/pci/drivers/uhci_hcd

    // echo '1-1.3' > sudo tee /sys/bus/pci/drivers/uhci_hcd/unbind

    // echo -n "1-3:1.1" > sudo tee /sys/bus/pci/drivers/xhci_hcd/unbind

    // echo -n "1-3:1.1" > sudo tee /sys/bus/pci/drivers/uhci_hcd/unbind

    // echo -n "0003:248A:8514.0004" > sudo tee /sys/bus/pci/drivers/xhci_hcd/unbind

   // 0003:248A:8514.0003

}


void MainWindow::on_cmd_functionButton_clicked()
{
    QDateTime current = QDateTime::currentDateTime();
    ui->tab_Main->setCurrentIndex(PageWindows::pageTime);
    ui->lineEdit_date_time->setText(current.toString("yyyy-MM-dd hh:mm:ss"));
}

// Transitek power control (cat /sys/class/gpio/gpio0/value)
void MainWindow::on_checkBox_transitek_stateChanged(int arg1)
{
    // disable transitek
    if(arg1 == 0)
    {
        shell_command("echo 0 > /sys/class/leds/ena_bskpwr/brightness");
        ui->cmd_Transitek->setEnabled(false);
        ui->cmb_fonarik->setEnabled(false);
    }
    else
    {
        shell_command("echo 1 > /sys/class/leds/ena_bskpwr/brightness");
        ui->cmd_Transitek->setEnabled(true);
        ui->cmb_fonarik->setEnabled(true);
    }
}

void MainWindow::on_checkBox_4g_stateChanged(int arg1)
{
    if(arg1 == 0)
    {
        // modem power OFF
        //echo 1 > /sys/class/leds/gsm_pwr_key/brightness # pwrbutton not pressed
        //echo 0 > /sys/class/leds/gsm_usb_boot/brightness # normal boot
        //sleep 1
        //echo 0 > /sys/class/leds/gsm_ena_gsmpwr/brightness #power on

        shell_command("echo 1 > /sys/class/leds/gsm_pwr_key/brightness");
        QThread::sleep(1);
        shell_command("echo 0 > /sys/class/leds/gsm_pwr_key/brightness");
        //shell_command("echo 0 > /sys/class/leds/gsm_usb_boot/brightness");
        QThread::sleep(3);
        shell_command("echo 0 > /sys/class/leds/gsm_ena_gsmpwr/brightness");
    }
    else
    {
        // modem power ON
        //echo 0 > /sys/class/leds/gsm_pwr_key/brightness # pwrbutton not pressed
        //echo 0 > /sys/class/leds/gsm_usb_boot/brightness # normal boot
        //sleep 1
        //echo 1 > /sys/class/leds/gsm_ena_gsmpwr/brightness #power on

        shell_command("echo 1 > /sys/class/leds/gsm_ena_gsmpwr/brightness");
        QThread::sleep(1);
        shell_command("echo 1 > /sys/class/leds/gsm_pwr_key/brightness");
        QThread::sleep(1);
        shell_command("echo 0 > /sys/class/leds/gsm_pwr_key/brightness");
        //shell_command("echo 0 > /sys/class/leds/gsm_usb_boot/brightness");
        //QThread::sleep(1);
        //shell_command("echo 1 > /sys/class/leds/gsm_ena_gsmpwr/brightness");

    }
}

void MainWindow::on_checkBox_wifi_stateChanged(int arg1)
{
    //    systemctl disable variscite-bt
    //    systemctl disable variscite-wifi

    if(arg1 == 0)
    {
        // OFF wi-fi and bluetooth
        shell_command("systemctl disable variscite-bt");
        shell_command("systemctl disable variscite-wifi");
        shell_command("nmcli radio wifi off");
    }
    else
    {
        shell_command("systemctl enable variscite-bt");
        shell_command("systemctl enable variscite-wifi");
        shell_command("nmcli radio wifi on");
    }
}

void MainWindow::on_cmd_connect_wifi_clicked()
{
    ui->tab_Main->setCurrentIndex(0);
}

// Кнопка настройки
void MainWindow::on_cmd_wifi_connect_clicked()
{
    // sudo systemctl restart ssh
    // sudo systemctl status ssh

    // nmcli connection show
    // QString.split(QRegExp("\n|\r\n|\r"));
    // QString.split(QRegExp("[\r\n]"),QString::SkipEmptyParts);

    // sudo ls -l /etc/NetworkManager/system-connections/

    // #delete connection (wificonn1)
    // nmcli con delete (CONNECTION_NAME)

    // goto page wifi settings
    waitShow();

    QString wifi_list = shell_command("nmcli -f SSID device wifi");
    QStringList wf_list = wifi_list.split(QRegExp("[\r\n]"),QString::SkipEmptyParts);

    ui->cmb_wifi_list->clear();
    ui->cmb_wifi_list->addItems(wf_list);

    ui->lineEditWiFi->setText("");

    waitHide();
    ui->tab_Main->setCurrentIndex(4);
}

// Кнопка Сохранить Wi-Fi Connection
void MainWindow::on_cmd_save_tab5_clicked()
{
    QString save_command = "nmcli c add type wifi con-name " + ui->cmb_wifi_list->currentText() + " ifname wlan0 ssid " + ui->cmb_wifi_list->currentText() + " wifi-sec.key-mgmt wpa-psk wifi-sec.psk " + ui->lineEditWiFi->text() +" ipv4.route-metric 50";
    // nmcli c add type wifi con-name wificonn1 ifname wlan0 ssid Keenetic-6779 wifi-sec.key-mgmt wpa-psk wifi-sec.psk pr9hpEVv ipv4.route-metric 50
    shell_command(save_command.toLatin1());
    ui->tab_Main->setCurrentIndex(0);

    ui->tab_Main->setCurrentIndex(0);
}

// Кнопка НАЗАД
void MainWindow::on_cmd_back_tab5_clicked()
{
    ui->tab_Main->setCurrentIndex(0);
}

void MainWindow::addTextWiFi(QString key)
{
    ui->lineEditWiFi->setText(ui->lineEditWiFi->text() + key);
}

void MainWindow::addTextDiteTime(QString key)
{
    ui->lineEdit_date_time->setText(ui->lineEdit_date_time->text() + key);
}


void MainWindow::on_checkBox_SSH_stateChanged(int arg1)
{
//    sudo systemctl enable ssh
//    sudo systemctl start ssh
//    sudo systemctl stop ssh
//    sudo systemctl disable ssh

    if(arg1 == 0)
    {
        shell_command("/etc/init.d/ssh stop");
        shell_command("systemctl stop ssh");
        shell_command("systemctl disable ssh");

    }
    else
    {
        shell_command("systemctl enable ssh");
        shell_command("/etc/init.d/ssh start");
        shell_command("systemctl start ssh");
    }
}

void MainWindow::on_cmd_delete_wifi_clicked()
{
    // nmcli --pretty --fields UUID,TYPE con show | grep wifi | awk "{print $1}" | while read line; do nmcli con delete uuid  $line; done
    // nmcli connection delete CONNECTION_NAME
    shell_command("nmcli --pretty --fields UUID,TYPE con show | grep wifi | awk \"{print $1}\" | while read line; do nmcli con delete uuid  $line; done");
}


void MainWindow::on_cmd_mem_sleep_clicked()
{
    // disable input devices freeze mem
    // evtest --grab /dev/input/event0 > /dev/null
    // evtest --grab /dev/input/event0 > /dev/null 2>&1 &
    //echo mem > /sys/power/state
    //echo freeze > /sys/power/state

//    this->repaint();
//    this->update();

//    shell_command("evtest --grab /dev/input/event0 > /dev/null 2>&1 &");

    this->repaint();
    this->update();

    shell_command("echo mem > /sys/power/state");
    //shell_command("echo mem > /sys/power/state & evtest --grab /dev/input/event0 > /dev/null 2>&1 &");

    this->repaint();
    this->update();

    //shell_command("pkill evtest");

    this->repaint();
    this->update();

    //shell_command("echo freeze > /sys/power/state");
}


void MainWindow::on_checkBox_USB_stateChanged(int arg1)
{
    if(arg1 == 0)
    {
        shell_command("echo 0 > /sys/class/leds/charger_board_kkt_connect/brightness");
    }
    else
    {
        shell_command("echo 1 > /sys/class/leds/charger_board_kkt_connect/brightness");
    }
}

void MainWindow::on_checkBox_KKT_On_stateChanged(int arg1)
{
    //waitShow();

    if(arg1 == 0)
    {
        shell_command("echo 0 > /sys/class/leds/charger_board_kkt_power/brightness");
        ui->cmd_KKT->setEnabled(false);
        ui->cmd_scanKKT->setEnabled(false);
        ui->cmdKKT_power_low->setEnabled(false);
        ui->cmd_send_OFD->setEnabled(false);
        ui->cmd_date_time_kkt->setEnabled(false);
        ui->cmd_date_time_kkt_2->setEnabled(false);
    }
    else
    {
        shell_command("echo 1 > /sys/class/leds/charger_board_kkt_power/brightness");
        ui->cmd_KKT->setEnabled(true);
        ui->cmd_scanKKT->setEnabled(true);
        ui->cmdKKT_power_low->setEnabled(true);
        ui->cmd_send_OFD->setEnabled(true);
        ui->cmd_date_time_kkt->setEnabled(true);
        ui->cmd_date_time_kkt_2->setEnabled(true);
    }

    //QThread::sleep(10);
    //waitHide(3);
}

void MainWindow::on_checkBox_Led_Green_stateChanged(int arg1)
{
    if(arg1 == 0)
    {
        shell_command("echo 0 > /sys/class/leds/charger_board_led_green/brightness");
    }
    else
    {
        shell_command("echo 1 > /sys/class/leds/charger_board_led_green/brightness");
    }
}

void MainWindow::on_checkBox_Led_Red_stateChanged(int arg1)
{
    if(arg1 == 0)
    {
        shell_command("echo 0 > /sys/class/leds/charger_board_led_red/brightness");
    }
    else
    {
        shell_command("echo 1 > /sys/class/leds/charger_board_led_red/brightness");
    }
}


void MainWindow::on_checkBox_bootloader_mode_stateChanged(int arg1)
{
    if(arg1 == 0)
    {
        shell_command("echo 0 > /sys/class/leds/charger_board_bootloader_mode/brightness");
    }
    else
    {
        shell_command("echo 1 > /sys/class/leds/charger_board_bootloader_modesfs/brightness");
    }
}


void MainWindow::on_checkBox_led_stateChanged(int arg1)
{
    if(arg1 == 0)
    {
        shell_command("echo 0 > /sys/class/leds/charger_board_led_power/brightness");
    }
    else
    {
        shell_command("echo 1 > /sys/class/leds/charger_board_led_power/brightness");
    }
}

void MainWindow::on_cmd_date_time_back_clicked()
{
    ui->tab_Main->setCurrentIndex(0);
}

void MainWindow::on_cmd_date_time_kkt_clicked()
{
        waitShow();
        // QString format = "yyyy-MM-dd HH:mm:ss";
        //QString txt = "";

        //char oprname[100];
        //int8_t isprint = 0;
        char emptystr[3];
        emptystr[0] = 0;
        char addreq[100] ; //=  "Доп. информация";

        int32_t retval;
        QString txt_info = "";
        char oprname[30] ; //s= "Иванов И. И.";
        char pswd[] = "AERF";

        char devname1[] = "/dev/ttySC0";
        char devname2[] = "/dev/rfcomm0";
        char devname3[] = "/dev/ttymxc2";

        char baudrate[] = "115200";
        int32_t flagoem = 1;

        if(flagoem == 0) {
            cpcurrent = cp866;
        }
        else {
            cpcurrent = cp1251;
        }

        tempqstr = "Иванов И. И.";
        std::strncpy(oprname, cpcurrent->fromUnicode(tempqstr).constData(), 30);

        tempqstr = "Доп. информация";
        std::strncpy(addreq, cpcurrent->fromUnicode(tempqstr).constData(), 30);

        qApp->processEvents();

//        if(ui->cmb_kkt->currentIndex() == 0)
//        {
//            retval = OpenDLLPlus(oprname, pswd, devname1, baudrate, flagoem);
//        }
//        else
//        {
//            retval = OpenDLLPlus(oprname, pswd, devname2, baudrate, flagoem);
//        }

        if(ui->cmb_kkt->currentIndex() == 0)
        {
        retval = OpenDLLPlus(oprname, pswd, devname1, baudrate, flagoem);
        }
        else if(ui->cmb_kkt->currentIndex() == 2)
        {
        retval = OpenDLLPlus(oprname, pswd, devname3, baudrate, flagoem);
        }
        else
        {
        retval = OpenDLLPlus(oprname, pswd, devname2, baudrate, flagoem);
        }

        txt_info += "OpenDLLPlus: " + QString::number(retval) + "\r";

        retval = GetDate();

        if((retval & 0xff) == 0) {
            char datestr[6];
            char timestr[4];
            GetFldStr(5,datestr);
            GetFldStr(6,timestr);

            QString format = "yyyy-MM-dd HH:mm:ss";
            QString datetimeString = "";
            QString timeString = QString::fromLatin1(timestr, 4);
            QString dateString = QString::fromLatin1(datestr, 6);

            dateString = "20" + dateString.mid(4,2)+ "-" + dateString.mid(2,2) + "-" + dateString.mid(0,2);
            timeString = timeString.mid(0,2) + ":" + timeString.mid(2,2) + ":00";

            datetimeString = dateString + " " + timeString;

            ui->lineEdit_date_time->setText(datetimeString);
        }

        //int res = SetDateEx(ui->lineEdit_date_time->text().split(' ')[0].toLatin1().data(), ui->lineEdit_date_time->text().split(' ')[1].toLatin1().data());
        //txt_info += "SetDateEx: " + QString::number(res)+ "\r";

        retval = CloseDLL();
        txt_info += "CloseDLL: " + QString::number(retval) + "\r";

        //showInfo(txt_info);
        waitHide(PageWindows::pageTime);
}

// set date and time system
void MainWindow::on_cmd_date_time_terminal_clicked()
{
//    date -s '2021-02-07 15:15' #change
//    hwclock -w -u
//    QString dt_text = ui->lineEdit_date_time->text();
//    //hwclock --set --date="2011-04-19 20:45:05"  --utc
//    QString dt = "hwclock --set --date=\"" +dt_text +"\"  --utc";
//    system(dt.toUtf8());
//    system("hwclock -w -u");

    QDateTime current = QDateTime::currentDateTime();
    ui->lineEdit_date_time->setText(current.toString("yyyy-MM-dd hh:mm:ss"));
}

// QT_QPA_PLATFORM=wayland
void MainWindow::on_cmd_paint_clicked()
{
    waitShow();
    QString txt = "";

    system("/root/mk100/mk100t/paint -platform wayland &");

    this->window()->setVisible(false);

    QThread::sleep(10);

    ui->txtText->setFontPointSize(16);

    for(int i = 0; i < 10; i++)
    {
        showInfo(gps_string);
        ui->tab_Main->setCurrentIndex(1);
        qApp->processEvents();
        QThread::sleep(1);
        qApp->processEvents();
    }

    txt += shell_command("pkill -f paint");

    this->window()->setVisible(true);

    ui->txtText->setFontPointSize(30);
    txt += "ТЕСТ ЭКРАНА ОКОНЧЕН";

    showInfo(txt);
    waitHide();
}


void MainWindow::on_cmd_send_OFD_clicked()
{
    waitShow();

    //QString txt = "";

    //char oprname[100];
    int8_t isprint = 0;
    char emptystr[3];
    emptystr[0] = 0;
    char addreq[100] ; //=  "Доп. информация";

    int32_t retval;
    QString txt_info = "";
    char oprname[30] ; //s= "Иванов И. И.";
    char pswd[] = "AERF";

    char devname1[] = "/dev/ttySC0";
    char devname2[] = "/dev/rfcomm0";
    char devname3[] = "/dev/ttymxc2";


    char baudrate[] = "115200";
    int32_t flagoem = 1;

    if(flagoem == 0) {
        cpcurrent = cp866;
    }
    else {
        cpcurrent = cp1251;
    }

    tempqstr = "Иванов И. И.";
    std::strncpy(oprname, cpcurrent->fromUnicode(tempqstr).constData(), 30);

    tempqstr = "Доп. информация";
    std::strncpy(addreq, cpcurrent->fromUnicode(tempqstr).constData(), 30);

    qApp->processEvents();

    if(ui->cmb_kkt->currentIndex() == 0)
    {
    retval = OpenDLLPlus(oprname, pswd, devname1, baudrate, flagoem);
    }
    else if(ui->cmb_kkt->currentIndex() == 2)
    {
    retval = OpenDLLPlus(oprname, pswd, devname3, baudrate, flagoem);
    }
    else
    {
    retval = OpenDLLPlus(oprname, pswd, devname2, baudrate, flagoem);
    }

//    if(ui->cmb_kkt->currentIndex() == 0)
//    {
//        retval = OpenDLLPlus(oprname, pswd, devname1, baudrate, flagoem);
//    }
//    else
//    {
//        retval = OpenDLLPlus(oprname, pswd, devname2, baudrate, flagoem);
//    }

    txt_info += "OpenDLLPlus: " + QString::number(retval) + "\r";

    LogEnable(1);
    QThread::sleep(1);

    retval = FNSendMessageOFD();
    txt_info += "FNSendMessageOFD: " + QString::number(retval) + "\r";

    QThread::sleep(1);

    retval = FNSendOFD();
    txt_info += "FNSendOFD: " + QString::number(retval) + "\r";

    retval = CloseDLL();
    txt_info += "CloseDLL: " + QString::number(retval) + "\r";

    showInfo(txt_info);
    waitHide();
}


// ====================================================
// ====================================================
// KEYBOARD START
// ====================================================
// ====================================================

void MainWindow::on_cmd_key_0_clicked()
{
    //addTextWiFi("0");

    if(capKeyBoard == true)
    {
        addTextWiFi("0");
    }
    else
    {
        addTextWiFi(")");
    }
}

void MainWindow::on_cmd_key_1_clicked()
{
    //addTextWiFi("1");

    if(capKeyBoard == true)
    {
        addTextWiFi("1");
    }
    else
    {
        addTextWiFi("!");
    }
}

void MainWindow::on_cmd_key_2_clicked()
{
    //addTextWiFi("2");

    if(capKeyBoard == true)
    {
        addTextWiFi("2");
    }
    else
    {
        addTextWiFi("@");
    }
}

void MainWindow::on_cmd_key_3_clicked()
{
    //addTextWiFi("3");

    if(capKeyBoard == true)
    {
        addTextWiFi("3");
    }
    else
    {
        addTextWiFi("#");
    }
}

void MainWindow::on_cmd_key_4_clicked()
{
    //addTextWiFi("4");

    if(capKeyBoard == true)
    {
        addTextWiFi("4");
    }
    else
    {
        addTextWiFi("$");
    }
}

void MainWindow::on_cmd_key_5_clicked()
{
    //addTextWiFi("5");

    if(capKeyBoard == true)
    {
        addTextWiFi("5");
    }
    else
    {
        addTextWiFi("%");
    }
}

void MainWindow::on_cmd_key_6_clicked()
{
    //addTextWiFi("6");

    if(capKeyBoard == true)
    {
        addTextWiFi("6");
    }
    else
    {
        addTextWiFi("^");
    }
}

void MainWindow::on_cmd_key_7_clicked()
{
    //addTextWiFi("7");

    if(capKeyBoard == true)
    {
        addTextWiFi("7");
    }
    else
    {
        addTextWiFi("&");
    }
}

void MainWindow::on_cmd_key_8_clicked()
{
    //addTextWiFi("8");

    if(capKeyBoard == true)
    {
        addTextWiFi("8");
    }
    else
    {
        addTextWiFi("*");
    }
}

void MainWindow::on_cmd_key_9_clicked()
{
    //addTextWiFi("9");

    if(capKeyBoard == true)
    {
        addTextWiFi("9");
    }
    else
    {
        addTextWiFi("(");
    }
}

void MainWindow::on_cmd_key_del_clicked()
{
    ui->lineEditWiFi->setText("");
}

void MainWindow::on_cmd_key_cap_look_clicked()
{
    if(capKeyBoard == true)
    {
        //
        ui->cmd_key_Q->setText("q");
        ui->cmd_key_W->setText("w");
        ui->cmd_key_E->setText("e");
        ui->cmd_key_R->setText("r");
        ui->cmd_key_T->setText("t");
        ui->cmd_key_Y->setText("y");
        ui->cmd_key_U->setText("u");
        ui->cmd_key_I->setText("i");
        ui->cmd_key_O->setText("o");
        ui->cmd_key_P->setText("p");
        ui->cmd_key_A->setText("a");
        ui->cmd_key_S->setText("s");
        ui->cmd_key_D->setText("d");
        ui->cmd_key_F->setText("f");
        ui->cmd_key_G->setText("g");
        ui->cmd_key_H->setText("h");
        ui->cmd_key_J->setText("j");
        ui->cmd_key_K->setText("k");
        ui->cmd_key_L->setText("l");
        ui->cmd_key_Z->setText("z");
        ui->cmd_key_X->setText("x");
        ui->cmd_key_C->setText("c");
        ui->cmd_key_V->setText("v");
        ui->cmd_key_B->setText("b");
        ui->cmd_key_N->setText("n");
        ui->cmd_key_M->setText("m");

        ui->cmd_key_dot->setText(",");
        ui->cmd_key_space->setText(" ");
        ui->cmd_key_min->setText("_");
        ui->cmd_key_dwoe->setText(";");

        ui->cmd_key_0->setText(")");
        ui->cmd_key_1->setText("!");
        ui->cmd_key_2->setText("@");
        ui->cmd_key_3->setText("#");
        ui->cmd_key_4->setText("$");
        ui->cmd_key_5->setText("%");
        ui->cmd_key_6->setText("^");
        ui->cmd_key_7->setText("&&");
        ui->cmd_key_8->setText("*");
        ui->cmd_key_9->setText("(");

    }
    else
    {
        //
        ui->cmd_key_Q->setText("Q");
        ui->cmd_key_W->setText("W");
        ui->cmd_key_E->setText("E");
        ui->cmd_key_R->setText("R");
        ui->cmd_key_T->setText("T");
        ui->cmd_key_Y->setText("Y");
        ui->cmd_key_U->setText("U");
        ui->cmd_key_I->setText("I");
        ui->cmd_key_O->setText("O");
        ui->cmd_key_P->setText("P");
        ui->cmd_key_A->setText("A");
        ui->cmd_key_S->setText("S");
        ui->cmd_key_D->setText("D");
        ui->cmd_key_F->setText("F");
        ui->cmd_key_G->setText("G");
        ui->cmd_key_H->setText("H");
        ui->cmd_key_J->setText("J");
        ui->cmd_key_K->setText("K");
        ui->cmd_key_L->setText("L");
        ui->cmd_key_Z->setText("Z");
        ui->cmd_key_X->setText("X");
        ui->cmd_key_C->setText("C");
        ui->cmd_key_V->setText("V");
        ui->cmd_key_B->setText("B");
        ui->cmd_key_N->setText("N");
        ui->cmd_key_M->setText("M");

        ui->cmd_key_dot->setText(".");
        ui->cmd_key_space->setText(" ");
        ui->cmd_key_min->setText("-");
        ui->cmd_key_dwoe->setText(":");

        ui->cmd_key_0->setText("0");
        ui->cmd_key_1->setText("1");
        ui->cmd_key_2->setText("2");
        ui->cmd_key_3->setText("3");
        ui->cmd_key_4->setText("4");
        ui->cmd_key_5->setText("5");
        ui->cmd_key_6->setText("6");
        ui->cmd_key_7->setText("7");
        ui->cmd_key_8->setText("8");
        ui->cmd_key_9->setText("9");
    }

    capKeyBoard = !capKeyBoard;
}

void MainWindow::on_cmd_key_Q_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("Q");
    }
    else
    {
        addTextWiFi("q");
    }
}

void MainWindow::on_cmd_key_W_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("W");
    }
    else
    {
        addTextWiFi("w");
    }
}

void MainWindow::on_cmd_key_E_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("E");
    }
    else
    {
        addTextWiFi("e");
    }
}

void MainWindow::on_cmd_key_R_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("R");
    }
    else
    {
        addTextWiFi("r");
    }
}

void MainWindow::on_cmd_key_T_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("T");
    }
    else
    {
        addTextWiFi("t");
    }
}

void MainWindow::on_cmd_key_Y_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("Y");
    }
    else
    {
        addTextWiFi("y");
    }
}

void MainWindow::on_cmd_key_U_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("U");
    }
    else
    {
        addTextWiFi("u");
    }
}

void MainWindow::on_cmd_key_I_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("I");
    }
    else
    {
        addTextWiFi("i");
    }
}

void MainWindow::on_cmd_key_O_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("O");
    }
    else
    {
        addTextWiFi("o");
    }
}

void MainWindow::on_cmd_key_P_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("P");
    }
    else
    {
        addTextWiFi("p");
    }
}

void MainWindow::on_cmd_key_A_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("A");
    }
    else
    {
        addTextWiFi("a");
    }
}

void MainWindow::on_cmd_key_S_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("S");
    }
    else
    {
        addTextWiFi("s");
    }
}

void MainWindow::on_cmd_key_D_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("D");
    }
    else
    {
        addTextWiFi("d");
    }
}

void MainWindow::on_cmd_key_F_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("F");
    }
    else
    {
        addTextWiFi("f");
    }
}

void MainWindow::on_cmd_key_G_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("G");
    }
    else
    {
        addTextWiFi("g");
    }
}

void MainWindow::on_cmd_key_H_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("H");
    }
    else
    {
        addTextWiFi("h");
    }
}

void MainWindow::on_cmd_key_J_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("J");
    }
    else
    {
        addTextWiFi("j");
    }
}

void MainWindow::on_cmd_key_K_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("K");
    }
    else
    {
        addTextWiFi("k");
    }
}

void MainWindow::on_cmd_key_L_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("L");
    }
    else
    {
        addTextWiFi("l");
    }
}

void MainWindow::on_cmd_key_Z_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("Z");
    }
    else
    {
        addTextWiFi("z");
    }
}

void MainWindow::on_cmd_key_X_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("X");
    }
    else
    {
        addTextWiFi("x");
    }
}

void MainWindow::on_cmd_key_C_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("C");
    }
    else
    {
        addTextWiFi("c");
    }
}

void MainWindow::on_cmd_key_V_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("V");
    }
    else
    {
        addTextWiFi("v");
    }
}

void MainWindow::on_cmd_key_B_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("B");
    }
    else
    {
        addTextWiFi("b");
    }
}

void MainWindow::on_cmd_key_N_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("N");
    }
    else
    {
        addTextWiFi("n");
    }
}

void MainWindow::on_cmd_key_M_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("M");
    }
    else
    {
        addTextWiFi("m");
    }
}

void MainWindow::on_cmd_key_dot_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi(".");
    }
    else
    {
        addTextWiFi(",");
    }
}

void MainWindow::on_cmd_key_space_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi(" ");
    }
    else
    {
        addTextWiFi(" ");
    }
}


void MainWindow::on_cmd_key_min_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi("-");
    }
    else
    {
        addTextWiFi("_");
    }
}

void MainWindow::on_cmd_key_dwoe_clicked()
{
    if(capKeyBoard == true)
    {
        addTextWiFi(":");
    }
    else
    {
        addTextWiFi(";");
    }
}

// ====================================================
// ====================================================
// KEYBOARD END
// ====================================================
// ====================================================


// ====================================================
// ====================================================
// KEYBOARD DATE TIME START
// ====================================================
// ====================================================

void MainWindow::on_cmd_date_time_1_clicked()
{
    addTextDiteTime("1");
}

void MainWindow::on_cmd_date_time_2_clicked()
{
    addTextDiteTime("2");
}

void MainWindow::on_cmd_date_time_3_clicked()
{
    addTextDiteTime("3");
}

void MainWindow::on_cmd_date_time_4_clicked()
{
    addTextDiteTime("4");
}

void MainWindow::on_cmd_date_time_5_clicked()
{
    addTextDiteTime("5");
}

void MainWindow::on_cmd_date_time_6_clicked()
{
    addTextDiteTime("6");
}

void MainWindow::on_cmd_date_time_7_clicked()
{
    addTextDiteTime("7");
}

void MainWindow::on_cmd_date_time_8_clicked()
{
    addTextDiteTime("8");
}

void MainWindow::on_cmd_date_time_9_clicked()
{
    addTextDiteTime("9");
}

void MainWindow::on_cmd_date_time_0_clicked()
{
    addTextDiteTime("0");
}

void MainWindow::on_cmd_date_time_del_clicked()
{
    ui->lineEdit_date_time->setText("");
}

void MainWindow::on_cmd_date_time_dot_clicked()
{
    addTextDiteTime("-");
}

void MainWindow::on_cmd_date_time_wdot_clicked()
{
    addTextDiteTime(":");
}

void MainWindow::on_cmd_date_time_del_2_clicked()
{
    addTextDiteTime(" ");
}

// ====================================================
// ====================================================
// KEYBOARD DATE TIME END
// ====================================================
// ====================================================

void MainWindow::on_cmd_date_time_terminal_2_clicked()
{
    //    date -s '2021-02-07 15:15' #change
    //    hwclock -w -u
        QString dt_text = ui->lineEdit_date_time->text();
        //hwclock --set --date="2011-04-19 20:45:05"  --utc
        QString dt = "date -s \'" + dt_text + "\'" ; // "hwclock --set --date=\"" +dt_text +"\"  --utc";

        qDebug() << "dt: " << dt;

        system(dt.toLatin1());
        system("hwclock -w -u");
}



void MainWindow::on_cmd_date_time_kkt_2_clicked()
{
    waitShow();
    // QString format = "yyyy-MM-dd HH:mm:ss";
    //QString txt = "";

    //char oprname[100];
    //int8_t isprint = 0;
    char emptystr[3];
    emptystr[0] = 0;
    char addreq[100] ; //=  "Доп. информация";

    int32_t retval;
    QString txt_info = "";
    char oprname[30] ; //s= "Иванов И. И.";
    char pswd[] = "AERF";

    char devname1[] = "/dev/ttySC0";
    char devname2[] = "/dev/rfcomm0";
    char devname3[] = "/dev/ttymxc2";

    char baudrate[] = "115200";
    int32_t flagoem = 1;

    if(flagoem == 0) {
        cpcurrent = cp866;
    }
    else {
        cpcurrent = cp1251;
    }

    tempqstr = "Иванов И. И.";
    std::strncpy(oprname, cpcurrent->fromUnicode(tempqstr).constData(), 30);

    tempqstr = "Доп. информация";
    std::strncpy(addreq, cpcurrent->fromUnicode(tempqstr).constData(), 30);

    qApp->processEvents();

    if(ui->cmb_kkt->currentIndex() == 0)
    {
    retval = OpenDLLPlus(oprname, pswd, devname1, baudrate, flagoem);
    }
    else if(ui->cmb_kkt->currentIndex() == 2)
    {
    retval = OpenDLLPlus(oprname, pswd, devname3, baudrate, flagoem);
    }
    else
    {
    retval = OpenDLLPlus(oprname, pswd, devname2, baudrate, flagoem);
    }

//    if(ui->cmb_kkt->currentIndex() == 0)
//    {
//        retval = OpenDLLPlus(oprname, pswd, devname1, baudrate, flagoem);
//    }
//    else
//    {
//        retval = OpenDLLPlus(oprname, pswd, devname2, baudrate, flagoem);
//    }

    txt_info += "OpenDLLPlus: " + QString::number(retval) + "\r";

    retval = GetDate();

    if((retval & 0xff) == 0) {
        QString format = "yyyy-MM-dd HH:mm:ss";
        QString datetimeString = "2022-06-13 11:00:00";
        datetimeString = ui->lineEdit_date_time->text();

        QDateTime a = QDateTime::fromString(datetimeString, format);
        //assert(a.isValid()); // fails
        if(a.isValid())
        {
            QString timeString = a.toString("HHmm");
            QString dateString = a.toString("ddMMyy");

            int res = SetDateEx(dateString.toLatin1().data(), timeString.toLatin1().data());
            txt_info += "SetDateEx: " + QString::number(res)+ "\r";
        }
        else
        {
            int res = SetDate();
            txt_info += "SetDate: " + QString::number(res)+ "\r";
        }
    }

    //int res = SetDateEx(ui->lineEdit_date_time->text().split(' ')[0].toLatin1().data(), ui->lineEdit_date_time->text().split(' ')[1].toLatin1().data());
    //txt_info += "SetDateEx: " + QString::number(res)+ "\r";

    retval = CloseDLL();
    txt_info += "CloseDLL: " + QString::number(retval) + "\r";

    qDebug() << "Debug on_cmd_date_time_kkt_2_clicked: " << txt_info;

    //showInfo(txt_info);
    waitHide(PageWindows::pageTime);
}

void MainWindow::on_cmb_fonarik_activated(const QString &arg1)
{

}

void MainWindow::on_cmdKKT_power_low_clicked()
{
    ////////////////////////////////////////////////////
        //char oprname[100];
        int8_t isprint = 0;
        char emptystr[3];
        emptystr[0] = 0;
        char addreq[100] ; //=  "Доп. информация";

        int32_t retval;
        QString txt_info = "";
        char oprname[30] ; //s= "Иванов И. И.";
        char pswd[] = "AERF";

        char devname1[] = "/dev/ttySC0";
        char devname2[] = "/dev/rfcomm0";
        char devname3[] = "/dev/ttymxc2";

        char baudrate[] = "115200";
        int32_t flagoem = 1;

        if(flagoem == 0) {
            cpcurrent = cp866;
        }
        else {
            cpcurrent = cp1251;
        }

        tempqstr = "Иванов И. И.";
        std::strncpy(oprname, cpcurrent->fromUnicode(tempqstr).constData(), 30);

        tempqstr = "Доп. информация";
        std::strncpy(addreq, cpcurrent->fromUnicode(tempqstr).constData(), 30);

        qApp->processEvents();

        if(ui->cmb_kkt->currentIndex() == 0)
        {
            retval = OpenDLLPlus(oprname, pswd, devname1, baudrate, flagoem);
        }
        else if(ui->cmb_kkt->currentIndex() == 2)
        {
            retval = OpenDLLPlus(oprname, pswd, devname3, baudrate, flagoem);
        }
        else
        {
            retval = OpenDLLPlus(oprname, pswd, devname2, baudrate, flagoem);
        }

//        if(ui->cmb_kkt->currentIndex() == 0)
//        {
//            retval = OpenDLLPlus(oprname, pswd, devname1, baudrate, flagoem);
//        }
//        else
//        {
//            retval = OpenDLLPlus(oprname, pswd, devname2, baudrate, flagoem);
//        }

        txt_info += "OpenDLLPlus: " + QString::number(retval) + "\r";

        retval = LowPowerState();

        QThread::sleep(2);
        retval = CheckHealth();
        QThread::sleep(1);
        retval = CheckHealth();

        int32_t ShiftState = 0;
        int32_t ShiftTime = 0;

        retval = GetShiftState(&ShiftState, &ShiftTime);

        //outaddline(utf8->toUnicode("Штрих-код: "), tempstr);

        qDebug() << "Debug GetShiftState" << QString::number(retval);
        qDebug() << "Debug ShiftTime" << QString::number(ShiftTime);
        qDebug() << "Debug ShiftState" << QString::number(ShiftState);

//        retval = BarcodeManager(1, 0);
//        qDebug() << "Debug GetShiftState 1" << QString::number(retval);
//        retval = BarcodeManager(2, 0);
//        qDebug() << "Debug GetShiftState 2" << QString::number(retval);
//        retval = BarcodeManager(3, 10000);
//        qDebug() << "Debug GetShiftState 3" << QString::number(retval);
//        retval = BarcodeManager(4, 0);
//        qDebug() << "Debug GetShiftState 4" << QString::number(retval);

//        retval = LowPowerState();

//        retval = CheckHealth();
//        QThread::sleep(1);
//        retval = CheckHealth();

        retval = CloseDLL();
        txt_info += "CloseDLL: " + QString::number(retval) + "\r";
    ////////////////////////////////////////////////////

}

void MainWindow::on_cmdPowerOffYes_clicked()
{
    shell_command("/root/poweroff.sh");

    shell_command("echo 1 > /sys/class/leds/gsm_pwr_key/brightness # pwrbutton press");
    QThread::sleep(1);
    shell_command("echo 0 > /sys/class/leds/gsm_pwr_key/brightness # pwrbutton not pressed");

    //shell_command("echo 1 > /sys/class/leds/ena_bskpwr/brightness");

    shell_command("echo 0 > /sys/class/leds/ena_bskpwr/brightness");

    shell_command("echo 0 > /sys/class/leds/ena_bskpwr/brightness");

    shell_command("echo 1 > /sys/class/leds/charger_board_power_off/brightness");

    shell_command("poweroff");

    this->close();
    qApp->exit(0);
}

void MainWindow::on_cmdPowerOffNo_clicked()
{
    ui->tab_Main->setCurrentIndex(PageWindows::pageMain);
}
