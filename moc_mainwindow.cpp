/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.8)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.8. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[118];
    char stringdata0[2770];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 12), // "updateHandle"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 5), // "state"
QT_MOC_LITERAL(4, 31, 9), // "gpsHandle"
QT_MOC_LITERAL(5, 41, 5), // "do_it"
QT_MOC_LITERAL(6, 47, 18), // "on_cmd_KKT_clicked"
QT_MOC_LITERAL(7, 66, 21), // "on_cmd_2dCode_clicked"
QT_MOC_LITERAL(8, 88, 24), // "on_cmd_Transitek_clicked"
QT_MOC_LITERAL(9, 113, 20), // "on_cmd_sound_clicked"
QT_MOC_LITERAL(10, 134, 23), // "on_cmd_internet_clicked"
QT_MOC_LITERAL(11, 158, 23), // "on_cmd_backMain_clicked"
QT_MOC_LITERAL(12, 182, 19), // "on_cmd_exit_clicked"
QT_MOC_LITERAL(13, 202, 22), // "on_cmd_sysInfo_clicked"
QT_MOC_LITERAL(14, 225, 22), // "on_cmd_asseler_clicked"
QT_MOC_LITERAL(15, 248, 23), // "on_cmd_bluetoth_clicked"
QT_MOC_LITERAL(16, 272, 25), // "on_cmd_internet_2_clicked"
QT_MOC_LITERAL(17, 298, 21), // "on_cmd_record_clicked"
QT_MOC_LITERAL(18, 320, 18), // "on_cmd_gps_clicked"
QT_MOC_LITERAL(19, 339, 22), // "on_cmb_light_activated"
QT_MOC_LITERAL(20, 362, 4), // "arg1"
QT_MOC_LITERAL(21, 367, 5), // "index"
QT_MOC_LITERAL(22, 373, 19), // "on_cmd_wifi_clicked"
QT_MOC_LITERAL(23, 393, 20), // "on_cmd_modem_clicked"
QT_MOC_LITERAL(24, 414, 22), // "on_cmd_scanKKT_clicked"
QT_MOC_LITERAL(25, 437, 20), // "on_cmb_kkt_activated"
QT_MOC_LITERAL(26, 458, 33), // "on_cmb_manager_internet_activ..."
QT_MOC_LITERAL(27, 492, 22), // "on_cmd_battery_clicked"
QT_MOC_LITERAL(28, 515, 22), // "on_cmd_lighten_clicked"
QT_MOC_LITERAL(29, 538, 28), // "on_cmd_UpdateAcceler_clicked"
QT_MOC_LITERAL(30, 567, 24), // "on_cmd_fullLight_clicked"
QT_MOC_LITERAL(31, 592, 24), // "on_cmb_fonarik_activated"
QT_MOC_LITERAL(32, 617, 21), // "on_cmd_exit_2_clicked"
QT_MOC_LITERAL(33, 639, 20), // "on_cmd_power_clicked"
QT_MOC_LITERAL(34, 660, 25), // "on_cmd_power_back_clicked"
QT_MOC_LITERAL(35, 686, 29), // "on_cmd_functionButton_clicked"
QT_MOC_LITERAL(36, 716, 34), // "on_checkBox_transitek_stateCh..."
QT_MOC_LITERAL(37, 751, 27), // "on_checkBox_4g_stateChanged"
QT_MOC_LITERAL(38, 779, 29), // "on_checkBox_wifi_stateChanged"
QT_MOC_LITERAL(39, 809, 27), // "on_cmd_connect_wifi_clicked"
QT_MOC_LITERAL(40, 837, 27), // "on_cmd_wifi_connect_clicked"
QT_MOC_LITERAL(41, 865, 24), // "on_cmd_back_tab5_clicked"
QT_MOC_LITERAL(42, 890, 20), // "on_cmd_key_0_clicked"
QT_MOC_LITERAL(43, 911, 20), // "on_cmd_key_1_clicked"
QT_MOC_LITERAL(44, 932, 20), // "on_cmd_key_2_clicked"
QT_MOC_LITERAL(45, 953, 20), // "on_cmd_key_3_clicked"
QT_MOC_LITERAL(46, 974, 20), // "on_cmd_key_4_clicked"
QT_MOC_LITERAL(47, 995, 20), // "on_cmd_key_5_clicked"
QT_MOC_LITERAL(48, 1016, 20), // "on_cmd_key_6_clicked"
QT_MOC_LITERAL(49, 1037, 20), // "on_cmd_key_7_clicked"
QT_MOC_LITERAL(50, 1058, 20), // "on_cmd_key_9_clicked"
QT_MOC_LITERAL(51, 1079, 20), // "on_cmd_key_8_clicked"
QT_MOC_LITERAL(52, 1100, 24), // "on_cmd_save_tab5_clicked"
QT_MOC_LITERAL(53, 1125, 22), // "on_cmd_key_del_clicked"
QT_MOC_LITERAL(54, 1148, 27), // "on_cmd_key_cap_look_clicked"
QT_MOC_LITERAL(55, 1176, 20), // "on_cmd_key_Q_clicked"
QT_MOC_LITERAL(56, 1197, 20), // "on_cmd_key_W_clicked"
QT_MOC_LITERAL(57, 1218, 20), // "on_cmd_key_E_clicked"
QT_MOC_LITERAL(58, 1239, 20), // "on_cmd_key_R_clicked"
QT_MOC_LITERAL(59, 1260, 20), // "on_cmd_key_T_clicked"
QT_MOC_LITERAL(60, 1281, 20), // "on_cmd_key_Y_clicked"
QT_MOC_LITERAL(61, 1302, 20), // "on_cmd_key_U_clicked"
QT_MOC_LITERAL(62, 1323, 20), // "on_cmd_key_I_clicked"
QT_MOC_LITERAL(63, 1344, 20), // "on_cmd_key_O_clicked"
QT_MOC_LITERAL(64, 1365, 20), // "on_cmd_key_P_clicked"
QT_MOC_LITERAL(65, 1386, 20), // "on_cmd_key_A_clicked"
QT_MOC_LITERAL(66, 1407, 20), // "on_cmd_key_S_clicked"
QT_MOC_LITERAL(67, 1428, 20), // "on_cmd_key_D_clicked"
QT_MOC_LITERAL(68, 1449, 20), // "on_cmd_key_F_clicked"
QT_MOC_LITERAL(69, 1470, 20), // "on_cmd_key_G_clicked"
QT_MOC_LITERAL(70, 1491, 20), // "on_cmd_key_H_clicked"
QT_MOC_LITERAL(71, 1512, 20), // "on_cmd_key_J_clicked"
QT_MOC_LITERAL(72, 1533, 20), // "on_cmd_key_K_clicked"
QT_MOC_LITERAL(73, 1554, 20), // "on_cmd_key_L_clicked"
QT_MOC_LITERAL(74, 1575, 20), // "on_cmd_key_Z_clicked"
QT_MOC_LITERAL(75, 1596, 20), // "on_cmd_key_X_clicked"
QT_MOC_LITERAL(76, 1617, 20), // "on_cmd_key_C_clicked"
QT_MOC_LITERAL(77, 1638, 20), // "on_cmd_key_V_clicked"
QT_MOC_LITERAL(78, 1659, 20), // "on_cmd_key_B_clicked"
QT_MOC_LITERAL(79, 1680, 20), // "on_cmd_key_N_clicked"
QT_MOC_LITERAL(80, 1701, 20), // "on_cmd_key_M_clicked"
QT_MOC_LITERAL(81, 1722, 28), // "on_checkBox_SSH_stateChanged"
QT_MOC_LITERAL(82, 1751, 22), // "on_cmd_key_dot_clicked"
QT_MOC_LITERAL(83, 1774, 24), // "on_cmd_key_space_clicked"
QT_MOC_LITERAL(84, 1799, 22), // "on_cmd_key_min_clicked"
QT_MOC_LITERAL(85, 1822, 23), // "on_cmd_key_dwoe_clicked"
QT_MOC_LITERAL(86, 1846, 26), // "on_cmd_delete_wifi_clicked"
QT_MOC_LITERAL(87, 1873, 24), // "on_cmd_mem_sleep_clicked"
QT_MOC_LITERAL(88, 1898, 28), // "on_checkBox_USB_stateChanged"
QT_MOC_LITERAL(89, 1927, 31), // "on_checkBox_KKT_On_stateChanged"
QT_MOC_LITERAL(90, 1959, 34), // "on_checkBox_Led_Green_stateCh..."
QT_MOC_LITERAL(91, 1994, 32), // "on_checkBox_Led_Red_stateChanged"
QT_MOC_LITERAL(92, 2027, 40), // "on_checkBox_bootloader_mode_s..."
QT_MOC_LITERAL(93, 2068, 28), // "on_checkBox_led_stateChanged"
QT_MOC_LITERAL(94, 2097, 29), // "on_cmd_date_time_back_clicked"
QT_MOC_LITERAL(95, 2127, 28), // "on_cmd_date_time_kkt_clicked"
QT_MOC_LITERAL(96, 2156, 33), // "on_cmd_date_time_terminal_cli..."
QT_MOC_LITERAL(97, 2190, 20), // "on_cmd_paint_clicked"
QT_MOC_LITERAL(98, 2211, 23), // "on_cmd_send_OFD_clicked"
QT_MOC_LITERAL(99, 2235, 26), // "on_cmd_date_time_1_clicked"
QT_MOC_LITERAL(100, 2262, 26), // "on_cmd_date_time_2_clicked"
QT_MOC_LITERAL(101, 2289, 26), // "on_cmd_date_time_3_clicked"
QT_MOC_LITERAL(102, 2316, 26), // "on_cmd_date_time_4_clicked"
QT_MOC_LITERAL(103, 2343, 26), // "on_cmd_date_time_5_clicked"
QT_MOC_LITERAL(104, 2370, 26), // "on_cmd_date_time_6_clicked"
QT_MOC_LITERAL(105, 2397, 28), // "on_cmd_date_time_del_clicked"
QT_MOC_LITERAL(106, 2426, 26), // "on_cmd_date_time_7_clicked"
QT_MOC_LITERAL(107, 2453, 26), // "on_cmd_date_time_8_clicked"
QT_MOC_LITERAL(108, 2480, 26), // "on_cmd_date_time_9_clicked"
QT_MOC_LITERAL(109, 2507, 26), // "on_cmd_date_time_0_clicked"
QT_MOC_LITERAL(110, 2534, 28), // "on_cmd_date_time_dot_clicked"
QT_MOC_LITERAL(111, 2563, 29), // "on_cmd_date_time_wdot_clicked"
QT_MOC_LITERAL(112, 2593, 35), // "on_cmd_date_time_terminal_2_c..."
QT_MOC_LITERAL(113, 2629, 30), // "on_cmd_date_time_del_2_clicked"
QT_MOC_LITERAL(114, 2660, 30), // "on_cmd_date_time_kkt_2_clicked"
QT_MOC_LITERAL(115, 2691, 27), // "on_cmdKKT_power_low_clicked"
QT_MOC_LITERAL(116, 2719, 25), // "on_cmdPowerOffYes_clicked"
QT_MOC_LITERAL(117, 2745, 24) // "on_cmdPowerOffNo_clicked"

    },
    "MainWindow\0updateHandle\0\0state\0gpsHandle\0"
    "do_it\0on_cmd_KKT_clicked\0on_cmd_2dCode_clicked\0"
    "on_cmd_Transitek_clicked\0on_cmd_sound_clicked\0"
    "on_cmd_internet_clicked\0on_cmd_backMain_clicked\0"
    "on_cmd_exit_clicked\0on_cmd_sysInfo_clicked\0"
    "on_cmd_asseler_clicked\0on_cmd_bluetoth_clicked\0"
    "on_cmd_internet_2_clicked\0"
    "on_cmd_record_clicked\0on_cmd_gps_clicked\0"
    "on_cmb_light_activated\0arg1\0index\0"
    "on_cmd_wifi_clicked\0on_cmd_modem_clicked\0"
    "on_cmd_scanKKT_clicked\0on_cmb_kkt_activated\0"
    "on_cmb_manager_internet_activated\0"
    "on_cmd_battery_clicked\0on_cmd_lighten_clicked\0"
    "on_cmd_UpdateAcceler_clicked\0"
    "on_cmd_fullLight_clicked\0"
    "on_cmb_fonarik_activated\0on_cmd_exit_2_clicked\0"
    "on_cmd_power_clicked\0on_cmd_power_back_clicked\0"
    "on_cmd_functionButton_clicked\0"
    "on_checkBox_transitek_stateChanged\0"
    "on_checkBox_4g_stateChanged\0"
    "on_checkBox_wifi_stateChanged\0"
    "on_cmd_connect_wifi_clicked\0"
    "on_cmd_wifi_connect_clicked\0"
    "on_cmd_back_tab5_clicked\0on_cmd_key_0_clicked\0"
    "on_cmd_key_1_clicked\0on_cmd_key_2_clicked\0"
    "on_cmd_key_3_clicked\0on_cmd_key_4_clicked\0"
    "on_cmd_key_5_clicked\0on_cmd_key_6_clicked\0"
    "on_cmd_key_7_clicked\0on_cmd_key_9_clicked\0"
    "on_cmd_key_8_clicked\0on_cmd_save_tab5_clicked\0"
    "on_cmd_key_del_clicked\0"
    "on_cmd_key_cap_look_clicked\0"
    "on_cmd_key_Q_clicked\0on_cmd_key_W_clicked\0"
    "on_cmd_key_E_clicked\0on_cmd_key_R_clicked\0"
    "on_cmd_key_T_clicked\0on_cmd_key_Y_clicked\0"
    "on_cmd_key_U_clicked\0on_cmd_key_I_clicked\0"
    "on_cmd_key_O_clicked\0on_cmd_key_P_clicked\0"
    "on_cmd_key_A_clicked\0on_cmd_key_S_clicked\0"
    "on_cmd_key_D_clicked\0on_cmd_key_F_clicked\0"
    "on_cmd_key_G_clicked\0on_cmd_key_H_clicked\0"
    "on_cmd_key_J_clicked\0on_cmd_key_K_clicked\0"
    "on_cmd_key_L_clicked\0on_cmd_key_Z_clicked\0"
    "on_cmd_key_X_clicked\0on_cmd_key_C_clicked\0"
    "on_cmd_key_V_clicked\0on_cmd_key_B_clicked\0"
    "on_cmd_key_N_clicked\0on_cmd_key_M_clicked\0"
    "on_checkBox_SSH_stateChanged\0"
    "on_cmd_key_dot_clicked\0on_cmd_key_space_clicked\0"
    "on_cmd_key_min_clicked\0on_cmd_key_dwoe_clicked\0"
    "on_cmd_delete_wifi_clicked\0"
    "on_cmd_mem_sleep_clicked\0"
    "on_checkBox_USB_stateChanged\0"
    "on_checkBox_KKT_On_stateChanged\0"
    "on_checkBox_Led_Green_stateChanged\0"
    "on_checkBox_Led_Red_stateChanged\0"
    "on_checkBox_bootloader_mode_stateChanged\0"
    "on_checkBox_led_stateChanged\0"
    "on_cmd_date_time_back_clicked\0"
    "on_cmd_date_time_kkt_clicked\0"
    "on_cmd_date_time_terminal_clicked\0"
    "on_cmd_paint_clicked\0on_cmd_send_OFD_clicked\0"
    "on_cmd_date_time_1_clicked\0"
    "on_cmd_date_time_2_clicked\0"
    "on_cmd_date_time_3_clicked\0"
    "on_cmd_date_time_4_clicked\0"
    "on_cmd_date_time_5_clicked\0"
    "on_cmd_date_time_6_clicked\0"
    "on_cmd_date_time_del_clicked\0"
    "on_cmd_date_time_7_clicked\0"
    "on_cmd_date_time_8_clicked\0"
    "on_cmd_date_time_9_clicked\0"
    "on_cmd_date_time_0_clicked\0"
    "on_cmd_date_time_dot_clicked\0"
    "on_cmd_date_time_wdot_clicked\0"
    "on_cmd_date_time_terminal_2_clicked\0"
    "on_cmd_date_time_del_2_clicked\0"
    "on_cmd_date_time_kkt_2_clicked\0"
    "on_cmdKKT_power_low_clicked\0"
    "on_cmdPowerOffYes_clicked\0"
    "on_cmdPowerOffNo_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
     116,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  594,    2, 0x0a /* Public */,
       4,    1,  597,    2, 0x0a /* Public */,
       5,    0,  600,    2, 0x0a /* Public */,
       6,    0,  601,    2, 0x08 /* Private */,
       7,    0,  602,    2, 0x08 /* Private */,
       8,    0,  603,    2, 0x08 /* Private */,
       9,    0,  604,    2, 0x08 /* Private */,
      10,    0,  605,    2, 0x08 /* Private */,
      11,    0,  606,    2, 0x08 /* Private */,
      12,    0,  607,    2, 0x08 /* Private */,
      13,    0,  608,    2, 0x08 /* Private */,
      14,    0,  609,    2, 0x08 /* Private */,
      15,    0,  610,    2, 0x08 /* Private */,
      16,    0,  611,    2, 0x08 /* Private */,
      17,    0,  612,    2, 0x08 /* Private */,
      18,    0,  613,    2, 0x08 /* Private */,
      19,    1,  614,    2, 0x08 /* Private */,
      19,    1,  617,    2, 0x08 /* Private */,
      22,    0,  620,    2, 0x08 /* Private */,
      23,    0,  621,    2, 0x08 /* Private */,
      24,    0,  622,    2, 0x08 /* Private */,
      25,    1,  623,    2, 0x08 /* Private */,
      26,    1,  626,    2, 0x08 /* Private */,
      26,    1,  629,    2, 0x08 /* Private */,
      27,    0,  632,    2, 0x08 /* Private */,
      28,    0,  633,    2, 0x08 /* Private */,
      29,    0,  634,    2, 0x08 /* Private */,
      30,    0,  635,    2, 0x08 /* Private */,
      31,    1,  636,    2, 0x08 /* Private */,
      32,    0,  639,    2, 0x08 /* Private */,
      33,    0,  640,    2, 0x08 /* Private */,
      34,    0,  641,    2, 0x08 /* Private */,
      35,    0,  642,    2, 0x08 /* Private */,
      36,    1,  643,    2, 0x08 /* Private */,
      37,    1,  646,    2, 0x08 /* Private */,
      38,    1,  649,    2, 0x08 /* Private */,
      39,    0,  652,    2, 0x08 /* Private */,
      40,    0,  653,    2, 0x08 /* Private */,
      41,    0,  654,    2, 0x08 /* Private */,
      42,    0,  655,    2, 0x08 /* Private */,
      43,    0,  656,    2, 0x08 /* Private */,
      44,    0,  657,    2, 0x08 /* Private */,
      45,    0,  658,    2, 0x08 /* Private */,
      46,    0,  659,    2, 0x08 /* Private */,
      47,    0,  660,    2, 0x08 /* Private */,
      48,    0,  661,    2, 0x08 /* Private */,
      49,    0,  662,    2, 0x08 /* Private */,
      50,    0,  663,    2, 0x08 /* Private */,
      51,    0,  664,    2, 0x08 /* Private */,
      52,    0,  665,    2, 0x08 /* Private */,
      53,    0,  666,    2, 0x08 /* Private */,
      54,    0,  667,    2, 0x08 /* Private */,
      55,    0,  668,    2, 0x08 /* Private */,
      56,    0,  669,    2, 0x08 /* Private */,
      57,    0,  670,    2, 0x08 /* Private */,
      58,    0,  671,    2, 0x08 /* Private */,
      59,    0,  672,    2, 0x08 /* Private */,
      60,    0,  673,    2, 0x08 /* Private */,
      61,    0,  674,    2, 0x08 /* Private */,
      62,    0,  675,    2, 0x08 /* Private */,
      63,    0,  676,    2, 0x08 /* Private */,
      64,    0,  677,    2, 0x08 /* Private */,
      65,    0,  678,    2, 0x08 /* Private */,
      66,    0,  679,    2, 0x08 /* Private */,
      67,    0,  680,    2, 0x08 /* Private */,
      68,    0,  681,    2, 0x08 /* Private */,
      69,    0,  682,    2, 0x08 /* Private */,
      70,    0,  683,    2, 0x08 /* Private */,
      71,    0,  684,    2, 0x08 /* Private */,
      72,    0,  685,    2, 0x08 /* Private */,
      73,    0,  686,    2, 0x08 /* Private */,
      74,    0,  687,    2, 0x08 /* Private */,
      75,    0,  688,    2, 0x08 /* Private */,
      76,    0,  689,    2, 0x08 /* Private */,
      77,    0,  690,    2, 0x08 /* Private */,
      78,    0,  691,    2, 0x08 /* Private */,
      79,    0,  692,    2, 0x08 /* Private */,
      80,    0,  693,    2, 0x08 /* Private */,
      81,    1,  694,    2, 0x08 /* Private */,
      82,    0,  697,    2, 0x08 /* Private */,
      83,    0,  698,    2, 0x08 /* Private */,
      84,    0,  699,    2, 0x08 /* Private */,
      85,    0,  700,    2, 0x08 /* Private */,
      86,    0,  701,    2, 0x08 /* Private */,
      87,    0,  702,    2, 0x08 /* Private */,
      88,    1,  703,    2, 0x08 /* Private */,
      89,    1,  706,    2, 0x08 /* Private */,
      90,    1,  709,    2, 0x08 /* Private */,
      91,    1,  712,    2, 0x08 /* Private */,
      92,    1,  715,    2, 0x08 /* Private */,
      93,    1,  718,    2, 0x08 /* Private */,
      94,    0,  721,    2, 0x08 /* Private */,
      95,    0,  722,    2, 0x08 /* Private */,
      96,    0,  723,    2, 0x08 /* Private */,
      97,    0,  724,    2, 0x08 /* Private */,
      98,    0,  725,    2, 0x08 /* Private */,
      99,    0,  726,    2, 0x08 /* Private */,
     100,    0,  727,    2, 0x08 /* Private */,
     101,    0,  728,    2, 0x08 /* Private */,
     102,    0,  729,    2, 0x08 /* Private */,
     103,    0,  730,    2, 0x08 /* Private */,
     104,    0,  731,    2, 0x08 /* Private */,
     105,    0,  732,    2, 0x08 /* Private */,
     106,    0,  733,    2, 0x08 /* Private */,
     107,    0,  734,    2, 0x08 /* Private */,
     108,    0,  735,    2, 0x08 /* Private */,
     109,    0,  736,    2, 0x08 /* Private */,
     110,    0,  737,    2, 0x08 /* Private */,
     111,    0,  738,    2, 0x08 /* Private */,
     112,    0,  739,    2, 0x08 /* Private */,
     113,    0,  740,    2, 0x08 /* Private */,
     114,    0,  741,    2, 0x08 /* Private */,
      31,    1,  742,    2, 0x08 /* Private */,
     115,    0,  745,    2, 0x08 /* Private */,
     116,    0,  746,    2, 0x08 /* Private */,
     117,    0,  747,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   20,
    QMetaType::Void, QMetaType::Int,   21,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   21,
    QMetaType::Void, QMetaType::Int,   21,
    QMetaType::Void, QMetaType::QString,   20,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   21,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   20,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->updateHandle((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->gpsHandle((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->do_it(); break;
        case 3: _t->on_cmd_KKT_clicked(); break;
        case 4: _t->on_cmd_2dCode_clicked(); break;
        case 5: _t->on_cmd_Transitek_clicked(); break;
        case 6: _t->on_cmd_sound_clicked(); break;
        case 7: _t->on_cmd_internet_clicked(); break;
        case 8: _t->on_cmd_backMain_clicked(); break;
        case 9: _t->on_cmd_exit_clicked(); break;
        case 10: _t->on_cmd_sysInfo_clicked(); break;
        case 11: _t->on_cmd_asseler_clicked(); break;
        case 12: _t->on_cmd_bluetoth_clicked(); break;
        case 13: _t->on_cmd_internet_2_clicked(); break;
        case 14: _t->on_cmd_record_clicked(); break;
        case 15: _t->on_cmd_gps_clicked(); break;
        case 16: _t->on_cmb_light_activated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 17: _t->on_cmb_light_activated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 18: _t->on_cmd_wifi_clicked(); break;
        case 19: _t->on_cmd_modem_clicked(); break;
        case 20: _t->on_cmd_scanKKT_clicked(); break;
        case 21: _t->on_cmb_kkt_activated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 22: _t->on_cmb_manager_internet_activated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 23: _t->on_cmb_manager_internet_activated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 24: _t->on_cmd_battery_clicked(); break;
        case 25: _t->on_cmd_lighten_clicked(); break;
        case 26: _t->on_cmd_UpdateAcceler_clicked(); break;
        case 27: _t->on_cmd_fullLight_clicked(); break;
        case 28: _t->on_cmb_fonarik_activated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 29: _t->on_cmd_exit_2_clicked(); break;
        case 30: _t->on_cmd_power_clicked(); break;
        case 31: _t->on_cmd_power_back_clicked(); break;
        case 32: _t->on_cmd_functionButton_clicked(); break;
        case 33: _t->on_checkBox_transitek_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 34: _t->on_checkBox_4g_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 35: _t->on_checkBox_wifi_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 36: _t->on_cmd_connect_wifi_clicked(); break;
        case 37: _t->on_cmd_wifi_connect_clicked(); break;
        case 38: _t->on_cmd_back_tab5_clicked(); break;
        case 39: _t->on_cmd_key_0_clicked(); break;
        case 40: _t->on_cmd_key_1_clicked(); break;
        case 41: _t->on_cmd_key_2_clicked(); break;
        case 42: _t->on_cmd_key_3_clicked(); break;
        case 43: _t->on_cmd_key_4_clicked(); break;
        case 44: _t->on_cmd_key_5_clicked(); break;
        case 45: _t->on_cmd_key_6_clicked(); break;
        case 46: _t->on_cmd_key_7_clicked(); break;
        case 47: _t->on_cmd_key_9_clicked(); break;
        case 48: _t->on_cmd_key_8_clicked(); break;
        case 49: _t->on_cmd_save_tab5_clicked(); break;
        case 50: _t->on_cmd_key_del_clicked(); break;
        case 51: _t->on_cmd_key_cap_look_clicked(); break;
        case 52: _t->on_cmd_key_Q_clicked(); break;
        case 53: _t->on_cmd_key_W_clicked(); break;
        case 54: _t->on_cmd_key_E_clicked(); break;
        case 55: _t->on_cmd_key_R_clicked(); break;
        case 56: _t->on_cmd_key_T_clicked(); break;
        case 57: _t->on_cmd_key_Y_clicked(); break;
        case 58: _t->on_cmd_key_U_clicked(); break;
        case 59: _t->on_cmd_key_I_clicked(); break;
        case 60: _t->on_cmd_key_O_clicked(); break;
        case 61: _t->on_cmd_key_P_clicked(); break;
        case 62: _t->on_cmd_key_A_clicked(); break;
        case 63: _t->on_cmd_key_S_clicked(); break;
        case 64: _t->on_cmd_key_D_clicked(); break;
        case 65: _t->on_cmd_key_F_clicked(); break;
        case 66: _t->on_cmd_key_G_clicked(); break;
        case 67: _t->on_cmd_key_H_clicked(); break;
        case 68: _t->on_cmd_key_J_clicked(); break;
        case 69: _t->on_cmd_key_K_clicked(); break;
        case 70: _t->on_cmd_key_L_clicked(); break;
        case 71: _t->on_cmd_key_Z_clicked(); break;
        case 72: _t->on_cmd_key_X_clicked(); break;
        case 73: _t->on_cmd_key_C_clicked(); break;
        case 74: _t->on_cmd_key_V_clicked(); break;
        case 75: _t->on_cmd_key_B_clicked(); break;
        case 76: _t->on_cmd_key_N_clicked(); break;
        case 77: _t->on_cmd_key_M_clicked(); break;
        case 78: _t->on_checkBox_SSH_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 79: _t->on_cmd_key_dot_clicked(); break;
        case 80: _t->on_cmd_key_space_clicked(); break;
        case 81: _t->on_cmd_key_min_clicked(); break;
        case 82: _t->on_cmd_key_dwoe_clicked(); break;
        case 83: _t->on_cmd_delete_wifi_clicked(); break;
        case 84: _t->on_cmd_mem_sleep_clicked(); break;
        case 85: _t->on_checkBox_USB_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 86: _t->on_checkBox_KKT_On_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 87: _t->on_checkBox_Led_Green_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 88: _t->on_checkBox_Led_Red_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 89: _t->on_checkBox_bootloader_mode_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 90: _t->on_checkBox_led_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 91: _t->on_cmd_date_time_back_clicked(); break;
        case 92: _t->on_cmd_date_time_kkt_clicked(); break;
        case 93: _t->on_cmd_date_time_terminal_clicked(); break;
        case 94: _t->on_cmd_paint_clicked(); break;
        case 95: _t->on_cmd_send_OFD_clicked(); break;
        case 96: _t->on_cmd_date_time_1_clicked(); break;
        case 97: _t->on_cmd_date_time_2_clicked(); break;
        case 98: _t->on_cmd_date_time_3_clicked(); break;
        case 99: _t->on_cmd_date_time_4_clicked(); break;
        case 100: _t->on_cmd_date_time_5_clicked(); break;
        case 101: _t->on_cmd_date_time_6_clicked(); break;
        case 102: _t->on_cmd_date_time_del_clicked(); break;
        case 103: _t->on_cmd_date_time_7_clicked(); break;
        case 104: _t->on_cmd_date_time_8_clicked(); break;
        case 105: _t->on_cmd_date_time_9_clicked(); break;
        case 106: _t->on_cmd_date_time_0_clicked(); break;
        case 107: _t->on_cmd_date_time_dot_clicked(); break;
        case 108: _t->on_cmd_date_time_wdot_clicked(); break;
        case 109: _t->on_cmd_date_time_terminal_2_clicked(); break;
        case 110: _t->on_cmd_date_time_del_2_clicked(); break;
        case 111: _t->on_cmd_date_time_kkt_2_clicked(); break;
        case 112: _t->on_cmb_fonarik_activated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 113: _t->on_cmdKKT_power_low_clicked(); break;
        case 114: _t->on_cmdPowerOffYes_clicked(); break;
        case 115: _t->on_cmdPowerOffNo_clicked(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 116)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 116;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 116)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 116;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
