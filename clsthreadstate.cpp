#include "clsthreadstate.h"

// ============================================
// ============================================
// ============================================
clsThreadState::clsThreadState(QString s): name (s)
{
}

void clsThreadState::run()
{
    QString state = "";
    QString beb = "";

    //shell_command("echo 1 > /sys/class/leds/ena_bskpwr/brightness");

    for(;;)
    {
        state = "";
        beb = "";
        //state += shell_command("lshw");

        //BT
        beb = shell_command("sdptool browse A4:CF:12:44:EB:D2 && echo $?");
        beb = beb.replace("\r", "");
        beb = beb.replace("\r", "");
        beb = beb.trimmed();
        beb = beb.mid(beb.length() - 1, 1);
        qDebug() << "beb BT" << beb;
        if( beb == "0")
        {
            state += "1;";
        }
        else
        {
            state += "0;";
        }

        //wi-fi
        beb = shell_command("ip addr show wlan0 | grep ""inet "" &&  echo $?");
        beb = beb.replace("\r", "");
        beb = beb.replace("\r", "");
        beb = beb.trimmed();
        beb = beb.mid(beb.length() - 1, 1);
        qDebug() << "beb WiFi" << beb;
        if( beb == "0")
        {
            state += "1;";
        }
        else
        {
            state += "0;";
        }

        //4G

        QString name4G = shell_command("ip addr | grep enx");
        //name4G = "4: enx7e428acc1153: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000 inet 192.168.225.45/24 brd 192.168.225.255 scope global dynamic noprefixroute enx7e428acc1153" ;
        int comPosN = name4G.indexOf("enx");
        name4G = name4G.mid(comPosN, 15);       
        QString commandR = "ip addr show " + name4G +" | grep ""inet "" && echo $?";
        beb = shell_command(commandR.toLatin1());
        beb = beb.replace("\r", "");
        beb = beb.replace("\r", "");
        beb = beb.trimmed();
        beb = beb.mid(beb.length() - 1, 1);
        qDebug() << "beb 4G" << beb;
        if( beb == "0")
        {
            state += "1;";
        }
        else
        {
            state += "0;";
        }

        //GPS
        beb = shell_command("hcitool rssi A4:CF:12:44:EB:D2");
        if(beb.trimmed() == "Not connected.")
        {
            state += "0;";
        }
        else
        {
            state += "1;";
        }

        //enx4abe099df845
        //state += shell_command("ip addr show enx62c3360ade1b | grep ""inet "" && echo $?");
        //state += shell_command("gpspipe -w -n 1 | grep -m 1 lon && echo $?");
        //state += shell_command("ihcitool rssi A4:CF:12:44:EB:D2");

        // date +"%m-%d-%Y %H:%M"
        state += shell_command("date +\"%d-%m-%Y %H:%M\"");
        state = state.replace("\r", "");
        state = state.replace("\r", "");
        state = state.trimmed();
        emit send(state);
        QThread::sleep(1);
    }
}


QString clsThreadState::shell_command(const char *input, int timeout, char isErrorRes)
{
  QProcess p;

  QString command = QString::fromLatin1(input);

  //p.start("/bin/sh", QStringList() << "-c" << command);

  p.start("/bin/bash", QStringList() << "-c" << command);

  //p.waitForFinished(timeout);

  if(timeout == -1)
  {
    p.waitForFinished(timeout);
  }
  else
  {
    QThread::sleep(timeout);
    p.terminate();
    p.kill();
  }

  //qDebug() << "run" << command;

  auto res = p.readAllStandardOutput();

  auto err = p.readAllStandardError();

  //qDebug() << "out" << res;

  //qDebug() << "err" << err;

  //addLog(res + "\r\n" + err);

  if(isErrorRes == 1)
  {
        err = "";
  }

  return res + "\r\n" + err;

}







// ============================================
// ============================================
// ============================================
clsThreadBT::clsThreadBT(QString s): name (s)
{
}

void clsThreadBT::run()
{
    QString state = "";
    QString beb = "";

    for(;;)
    {
        state = "";
        beb = "";
        //state += shell_command("lshw");

        //BT
        beb = shell_command("sdptool browse A4:CF:12:44:EB:D2 && echo $?");
        beb = beb.replace("\r", "");
        beb = beb.replace("\r", "");
        beb = beb.trimmed();
        beb = beb.mid(beb.length() - 1, 1);
        qDebug() << "beb BT" << beb;
        if( beb == "0")
        {
            state += "1;";
        }
        else
        {
            state += "0;";
        }

        //wi-fi
        beb = shell_command("ip addr show wlan0 | grep ""inet "" &&  echo $?");
        beb = beb.replace("\r", "");
        beb = beb.replace("\r", "");
        beb = beb.trimmed();
        beb = beb.mid(beb.length() - 1, 1);
        qDebug() << "beb WiFi" << beb;
        if( beb == "0")
        {
            state += "1;";
        }
        else
        {
            state += "0;";
        }

        //4G

        QString name4G = shell_command("ip addr | grep enx");
        //name4G = "4: enx7e428acc1153: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000 inet 192.168.225.45/24 brd 192.168.225.255 scope global dynamic noprefixroute enx7e428acc1153" ;
        int comPosN = name4G.indexOf("enx");
        name4G = name4G.mid(comPosN, 15);
        QString commandR = "ip addr show " + name4G +" | grep ""inet "" && echo $?";
        beb = shell_command(commandR.toLatin1());
        beb = beb.replace("\r", "");
        beb = beb.replace("\r", "");
        beb = beb.trimmed();
        beb = beb.mid(beb.length() - 1, 1);
        qDebug() << "beb 4G" << beb;
        if( beb == "0")
        {
            state += "1;";
        }
        else
        {
            state += "0;";
        }

        //GPS
        beb = shell_command("hcitool rssi A4:CF:12:44:EB:D2");
        if(beb.trimmed() == "Not connected.")
        {
            state += "0;";
        }
        else
        {
            state += "1;";
        }

        //enx4abe099df845
        //state += shell_command("ip addr show enx62c3360ade1b | grep ""inet "" && echo $?");
        //state += shell_command("gpspipe -w -n 1 | grep -m 1 lon && echo $?");
        //state += shell_command("ihcitool rssi A4:CF:12:44:EB:D2");

        // date +"%m-%d-%Y %H:%M"
        state += shell_command("date +\"%m-%d-%Y %H:%M\"");
        state = state.replace("\r", "");
        state = state.replace("\r", "");
        state = state.trimmed();
        emit send(state);
        QThread::sleep(1);
    }
}


QString clsThreadBT::shell_command(const char *input, int timeout, char isErrorRes)
{
  QProcess p;

  QString command = QString::fromLatin1(input);

  //p.start("/bin/sh", QStringList() << "-c" << command);

  p.start("/bin/bash", QStringList() << "-c" << command);

  //p.waitForFinished(timeout);

  if(timeout == -1)
  {
    p.waitForFinished(timeout);
  }
  else
  {
    QThread::sleep(timeout);
    p.terminate();
    p.kill();
  }

  //qDebug() << "run" << command;

  auto res = p.readAllStandardOutput();

  auto err = p.readAllStandardError();

  //qDebug() << "out" << res;

  //qDebug() << "err" << err;

  //addLog(res + "\r\n" + err);

  if(isErrorRes == 1)
  {
        err = "";
  }

  return res + "\r\n" + err;

}


















// ============================================
// ============================================
// ============================================
clsThreadGPS::clsThreadGPS(QString s): name (s)
{
}

void clsThreadGPS::run()
{
    QString state = "";

    //shell_command("echo 1 > /sys/class/leds/ena_bskpwr/brightness");

    for(;;)
    {
        state = shell_command("gpspipe -w -n 10");
        qDebug() << "state GPS" << state << state;;
        emit send(state);
        QThread::sleep(1);
    }
}


QString clsThreadGPS::shell_command(const char *input, int timeout, char isErrorRes)
{
  QProcess p;
  QString command = QString::fromLatin1(input);
  //p.start("/bin/sh", QStringList() << "-c" << command);
  p.start("/bin/bash", QStringList() << "-c" << command);
  //p.waitForFinished(timeout);

  if(timeout == -1)
  {
    p.waitForFinished(timeout);
  }
  else
  {
    QThread::sleep(timeout);
    p.terminate();
    p.kill();
  }

  //qDebug() << "run" << command;
  auto res = p.readAllStandardOutput();
  auto err = p.readAllStandardError();

  //qDebug() << "out" << res;
  //qDebug() << "err" << err;
  //addLog(res + "\r\n" + err);

  if(isErrorRes == 1)
  {
        err = "";
  }

  return res + "\r\n" + err;

}







// ============================================
// ============================================
// ============================================
clsThread4G::clsThread4G(QString s): name (s)
{
}

void clsThread4G::run()
{
}


QString clsThread4G::shell_command(const char *input, int timeout, char isErrorRes)
{
}
