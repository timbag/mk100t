QT += core gui widgets serialport
QT += widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QT_IM_MODULE=qtvirtualkeyboard
#unset QT_IM_MODULE

QTPLUGIN += qtvirtualkeyboardplugin

CONFIG += c++11
CONFIG+=test

QMAKE_CXXFLAGS  += -finput-charset=utf-8
QMAKE_CXXFLAGS  += -fexec-charset=utf-8

QMAKE_CXXFLAGS  += -Wno-unused-parameter
QMAKE_CXXFLAGS  += -Wno-unused-variable

#QMAKE_LFLAGS += -Wl,--add-stdcall-alias

INCLUDEPATH += azimuth_qt5/include/

# arm
LIBS += -L"." -lAzimuth_Qt_5_9

# arm
#LIBS += -L"/home/vadim/projects/git_projects/mk100t" -lAzimuth_Qt_5_9

# x64
#LIBS += -L"/home/vadim/_archive_/azimuth_Qt/build-Azimuth_Qt_5_9-Desktop_Qt_5_10_1_GCC_64bit-11" -lAzimuth_Qt_5_9

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    clsthreadstate.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    clsthreadstate.h \
    mainwindow.h

FORMS += \
    mainwindow.ui

#include(QtConsole_/qtconsole.pri)

# Default rules for deployment.
#qnx: target.path = /tmp/$${TARGET}/bin
#else: unix:!android: target.path = /opt/$${TARGET}/bin
#!isEmpty(target.path): INSTALLS += target

target.files = MK100T   # your local executable file.
target.path = /root/mk100  # executable file's path on your device(beaglebone). there is no slash in the end of the path.
INSTALLS += target


