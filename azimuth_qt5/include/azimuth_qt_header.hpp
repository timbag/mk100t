

// Azimuth Qt library header with functions defines for gcc compilator

//---------------------------------------
#ifndef AZIMUTH_QT_5_9_LIBRARY_IMPORT
//---------------------------------------


#define AZIMUTH_QT_5_9_LIBRARY_IMPORT


#define PLATFORM_32BIT


#define AZQT_BFR_1   extern
#define AZQT_BFR_2   "C"
#define AZQT_BFR_3  


#if defined(PLATFORM_32BIT)
    #define AZQT_AFTER_TYPE     __attribute__((stdcall))
#else
    #define AZQT_AFTER_TYPE  
#endif






// ---------- AZIMUTH FUNCTIONS ----------




// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
AddArt(int32_t ArtNum, int32_t ArtFlag, char *ArtName);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
AddDept(int32_t DepNum, char *DepName);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
AddFreeField(int16_t Line, int16_t Col, int8_t Font, int8_t PrintMode, int8_t JourNo, char *Info);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
AddFreeField77(int16_t Line, int16_t Col, int8_t Font, int8_t PrintMode, int8_t JourNo, char *Info);



// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
AddFreeFieldPlus(int16_t Line, int16_t Col, int8_t Font,
                int8_t PrintMode, int8_t JourNo, char *Info);

                
                
// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
AddPayFieldPlus(int16_t Line, int16_t Col, int8_t Font,
                int8_t PayMode, int32_t Sum);
                
                
                
// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
AddPayFieldPlusEx(int16_t Line, int16_t Col, int8_t Font,
                int8_t PayMode, char *Sum);
                
                


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
AddPosField(int16_t SerNoLine, int16_t SerNoCol, int8_t SerNoFont,
            int16_t DocNoLine, int16_t DocNoCol, int8_t DocNoFont,
            int16_t DateLine, int16_t DateCol, int8_t DateFont,
            int16_t TimeLine, int16_t TimeCol, int8_t TimeFont,
            int16_t InnLine, int16_t InnCol, int8_t InnFont,
            int16_t OperLine, int16_t OperCol, int8_t OperFont,
            int16_t SumLine, int16_t SumCol, int8_t SumFont);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
AddPosField77(int16_t SerNoLine, int16_t SerNoCol, int8_t SerNoFont,
            int16_t DocNoLine, int16_t DocNoCol, int8_t DocNoFont,
            int16_t DateLine, int16_t DateCol, int8_t DateFont,
            int16_t TimeLine, int16_t TimeCol, int8_t TimeFont,
            int16_t InnLine, int16_t InnCol, int8_t InnFont,
            int16_t OperationLine, int16_t OperationCol, int8_t OperationFont,
            int16_t KPKLine, int16_t KPKCol, int8_t KPKFont,
            int16_t OperLine, int16_t OperCol, int8_t OperFont,
            int16_t SumLine, int16_t SumCol, int8_t SumFont);

            
            

// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
AddPosFieldPlus(int16_t SerNoLine, int16_t SerNoCol, int8_t SerNoFont,
                int16_t DocNoLine, int16_t DocNoCol, int8_t DocNoFont,
                int16_t OperNoLine, int16_t OperNoCol, int8_t OperNoFont,
                int16_t DateLine, int16_t DateCol, int8_t DateFont,
                int16_t TimeLine, int16_t TimeCol, int8_t TimeFont,
                int16_t InnLine, int16_t InnCol, int8_t InnFont,
                int16_t OperLine, int16_t OperCol, int8_t OperFont,
                int16_t DepLine, int16_t DepCol, int8_t DepFont,
                int16_t ArtLine, int16_t ArtCol, int8_t ArtFont,
                int16_t SumLine, int16_t SumCol, int8_t SumFont);
            
            
            
// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
AddPosFieldPlus1(int16_t SerNoLine, int16_t SerNoCol, int8_t SerNoFont,
                int16_t DocNoLine, int16_t DocNoCol, int8_t DocNoFont,
                int16_t OperNoLine, int16_t OperNoCol, int8_t OperNoFont,
                int16_t DateLine, int16_t DateCol, int8_t DateFont,
                int16_t TimeLine, int16_t TimeCol, int8_t TimeFont);
                
                
                
                
// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
AddPosFieldPlus2(int16_t InnLine, int16_t InnCol, int8_t InnFont,
                int16_t OperLine, int16_t OperCol, int8_t OperFont,
                int16_t DepLine, int16_t DepCol, int8_t DepFont,
                int16_t ArtLine, int16_t ArtCol, int8_t ArtFont,
                int16_t SumLine, int16_t SumCol, int8_t SumFont);
                
                
            
// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
AddSubDepFieldPlus(int8_t SubDepNum, int32_t Sum);
            
            
            
// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
AddSubDepFieldPlusex(int8_t SubDepNum, char *Sum);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
AddTLV(int16_t TLVID, char *TLV);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
BarcodeReceipt(int8_t BarcodeType, int8_t HRI, int8_t Font, int8_t Height, int8_t Width, char *Barcode);
            

            
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
BootStart(void);
            

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
CancelReceipt(void);
            


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 void AZQT_AFTER_TYPE
CashDriverEnable(int8_t IsEnable);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
CashDriverOpen(void);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ChangeBaudrate(char *BaudRate);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ChangeFiscalDocSum(int32_t Sum);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ChangeOpName(char *OpName);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ChangeService(int8_t ServiceType, char *OperatorPosition);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
CheckBit(int32_t Val, int32_t BitNum);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
CheckHealth(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
CheckMask(int32_t Val, int32_t Mask);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
CheckStatusNum(int32_t Num, int32_t BitNum);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ClearAllDept(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ClearDept(int8_t DepNo);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
CloseArt(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
CloseDept(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
CloseDLL(void);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
CloseFDoc(void);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
CloseFDocEx(char *Information, int32_t Len);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
CloseFDocPlus(void);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
CloseFreeDoc(void);

// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
CloseFreeDoc77(void);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
CloseFreeDocPlus(void);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
CloseReceipt(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
CloseReceiptNF(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ComissionReceipt(int8_t OType, int32_t Percent, int32_t Sum);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ComissionReceiptEx(int8_t OType, int32_t Percent, char *Sum, char *FreeField);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ComissionReceiptExx(int8_t OType, char *Percent, char *Sum, char *FreeField);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ComissionReceiptPlus(int8_t OType, int32_t Percent, int32_t Sum, char *FreeField);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
CommentReceipt(char *Buf);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
CorrectionTicket(int8_t DocType, int8_t Copies, int8_t Taxation, char *CashRegNo, char *Total, char *Cash,
                    char *NonCash, char *SubParam, char *AdditP);
                    
                    
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
CorrectionTicket105(int8_t DocType, int8_t Copies, int8_t Taxation, int8_t CorrectionType, int8_t TaxIndex,
                    char *CashRegNo, char *Total, char *Cash, char *NonCash, char *Avans, char *Credit,
                    char *Other, char *CorrectionDesc, char *CorrectionNumber, int32_t CorrectionDate);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
CutFDoc(void);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
DllComWrite(char *P, int32_t Count);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
DllComWritePlus(char *P, int32_t Count);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
DownloadGraphHeader(char *FName);



// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 bool AZQT_AFTER_TYPE
DownLoadHex(char MemoryChar, int64_t StartL, int64_t CountO, char *BufferP);



// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 bool AZQT_AFTER_TYPE
DownLoadHexLPC(char MemoryChar, int64_t StartL, int64_t CountO, char *BufferP);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
EJDateTimeReport(char *StartTime, char *EndTime, char *StartDate);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
EJErase(void);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
EJJournalToFile(int32_t ZReportNo, char *FileName);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
EJNoDoc(int16_t DocNo);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
EJNoDocEx(int16_t DocNo, int8_t OverCount);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
EJNoReport(int16_t StartNo, int16_t EndNo);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
EJNoReportEx(int16_t StartNo, int16_t EndNo, int8_t OverCount);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
EJPrint(void);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
EJPrintEx(int16_t ShiftNum);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
EJPrintFromFile(int32_t isZReport, char *FileName);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
EJPrintFromFileEx(int32_t prnType, int32_t isZReport, char *FileName);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
EJReadPage(int16_t PageNo);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
EJReadPageEx(char *PageNo);




AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
EJTimeReport(char *StartTime, char *EndTime);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
EJZReport(int32_t ZNum);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
EJZToFile(int32_t ZReportNo, char *FileName);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
EKLActivization(void);



// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
EKLActivizationReport(void);



// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
EKLClose(void);



// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
EKLEJournalReport(int32_t EJournalNum);



// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
EKLEJournalTotal(int32_t EJournalNum);




// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
EKLKPKReport(char *KPKNum);




// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
EKLShiftsDateReport(int32_t ReportType, char *StartDate, char *EndDate);



// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
EKLShiftsNumReport(int32_t ReportType, int32_t StartNo, int32_t EndNo);





AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ExceptToLog(char *Msg);



// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
Fiscalization(char *OldPass, char *NewPass, char *NewRegNo, char *NewCode, int8_t Group, int8_t TotalInFlash);





AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
Fiscalization105(char *CompanyName, char *CompanyINN, char *CompanyMail, char *RegNo, char *AvtomatNo, char *Cashier,
                char *PayAddr, char *PayPlace, char *OFDINN, char *OFDName, char *RegNum, char *FNS_Web,
                int8_t Taxation, int16_t Modes, int8_t ReRegCode, int8_t IsInternet, int8_t AgentType, int8_t SubParam);
                
                
                
                
// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
FiscalizationNew(char *CompanyName, char *CompanyINN, char *RegNo, char *AvtomatNo, char *Cashier, char *PayPlace,
                char *OFDINN, char *RegNum, char *SubParam, int8_t Taxation, int8_t Modes, int8_t ReRegCode,
                int8_t IsInternet);



// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
Fiscalization11(char *CompanyName, char *CompanyINN, char *CompanyMail, char *RegNo, char *AvtomatNo, char *Cashier,
                char *PayPlace, char *OFDINN, char *OFDName, char *RegNum, char *FNS_Web,
                int8_t Taxation, int8_t Modes, int8_t Modes2, int8_t ReRegCode, int8_t IsInternet, int8_t AgentType,
                int8_t SubParam, int32_t ReRegReason);
                
                

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
FNClose(char *Cashier);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
FNGetCommStatus(void);
                
                
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
FNGetDoc(int32_t Number, int8_t Flags);
                
                
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
FNGetError(void);
                
                
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
FNGetStatus(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
FNGetTotal(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
FNGetTotalEx(int8_t isPrint);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
FNGetTotalExx(int8_t Index, int8_t isPrint);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
FNRead(int16_t Offset, int16_t Size);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
FNReadCancel(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
FNReadClose(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
FNReadScript(char *OFDScript, int16_t Len);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
FNReadStart(void);



// additional function to azimuth.dll
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
FNSendMessageOFD(void);


// additional function to azimuth.dll
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
FNSendOFD(void);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
FontSelectFDoc(int8_t B);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
FreeDoc(char *Information, int16_t Len);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
FreeDocCut(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
FreeDocCutPlus(int8_t Count);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
FromCash(int32_t Sum);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
FromCashEx(char *Sum, char *FreeField);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
FromCashPlus(int32_t Sum, char *FreeField);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 char * AZQT_AFTER_TYPE
GetAnswer(char *PtrCommand);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetAnswerSize(void);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetArt(int32_t DepNum);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 char * AZQT_AFTER_TYPE
GetCommand(char *PtrCommand);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetCommandSize(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetCommStatus(int32_t *cbRX, int32_t *cbTX);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetCounters(void);



// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetCurrency(int8_t CurrencyNum);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetDate(void);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetDateReport(int8_t ReportType, char *Pass, char *StartDate, char *EndDate);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetDept(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 char * AZQT_AFTER_TYPE
GetDllVer(char *DllVer);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetDrawerParam(void);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetECRTCP(void);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetEJDump(int32_t PageNo);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetEJInfo(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetEJParam(void);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetEReport(int8_t ReportNum, int8_t Param);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 char * AZQT_AFTER_TYPE
GetErrorMessage(char *P);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 char * AZQT_AFTER_TYPE
GetErrorMessageEx(char *P, int32_t ErrNo);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 char * AZQT_AFTER_TYPE
GetErrorMessageNo(char *P, int32_t ErrNo);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetFiscalInfo(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetFiscalNums(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetFiscalParam(int16_t Teg);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 char * AZQT_AFTER_TYPE
GetFldArr(int8_t Num, char *Field);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 uint32_t AZQT_AFTER_TYPE
GetFldByte(int8_t Num);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 uint32_t AZQT_AFTER_TYPE
GetFldDWord(int8_t Num);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 float AZQT_AFTER_TYPE
GetFldFloat(int8_t Num);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 uint32_t AZQT_AFTER_TYPE
GetFldInt(int8_t Num);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 uint64_t AZQT_AFTER_TYPE
GetFldInt64(int8_t Num);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetFldsCount(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 char * AZQT_AFTER_TYPE
GetFldStr(int8_t Num, char *Field);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 char * AZQT_AFTER_TYPE
GetFldStrPrim(int8_t Num, char *Field);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 uint16_t AZQT_AFTER_TYPE
GetFldWord(int8_t Num);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 char * AZQT_AFTER_TYPE
GetFWVer(char *FWVer);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetLastAnswer(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetLastCommandNum(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetLastDllError(void);



// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetLastDoc(char *KPKNum);



// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetLastKPK(void);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetMoney(void);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetMony(void);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetNumbers(void);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetNumReport(int8_t ReportType, char *Pass, int16_t StartNo, int16_t EndNo);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetOFDTicket(int32_t Number, int8_t Flags);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetPaperLengthBeforeEndFDoc(char *P);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetPaperLengthFDoc(char *P);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetParamDoc(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetParameters(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetPayment(int8_t PaymentNum);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 uint8_t AZQT_AFTER_TYPE
GetPrnByte(int8_t Num);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetResource(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetSerialAnswer(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetSerialNum(void);



// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetShiftCloseTime(void);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetShiftState(int32_t *ShiftState, int32_t *ShiftTime);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetStatus(void);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
LowPowerState(void);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
BarcodeManager(int8_t cmd, int16_t Param);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetStatusNo(int8_t No);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetStatusNoPlus(int8_t No);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetStatusNum(int32_t Num);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetStatusPlus(void);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetTaxes(int8_t TIndex);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetTaxesPlus(int8_t TIndex, char *FreeField);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetTCPDelay(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetWaitDocs(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GetWEB(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GhangeService(int8_t ServiceType, char *OperatorPosition);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GNRunCommand(int8_t CmdNum, char *CmdI, char *CmdO);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
GRunCommand(int8_t CmdNum, char *CmdI);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
InitDisplay(void);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ItemDepReceipt(int8_t SecID, int8_t WareName, int8_t TaxID, int32_t Price, int32_t Count,
                char *Measure, char *WareCode);

                
// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ItemDepReceiptPlus(int8_t SecID, int8_t WareName, int8_t TaxID, int32_t Price, int32_t Count,
                char *Measure, char *WareCode, char *FreeField);
                
                

// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ItemDepReceiptEx(int8_t SecID, int8_t WareName, int8_t TaxID, char *Price, int32_t Count,
                char *Measure, char *WareCode, char *FreeField);
                
                
                
// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ItemDepReceiptExx(int8_t SecID, int8_t WareName, int8_t TaxID, char *Price, char *Count,
                char *Measure, char *WareCode, char *FreeField);
                
                

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ItemReceipt(char *WareName, char *WareCode, char *Measure, char *SecID, int32_t Price, int32_t Count, int16_t WareType);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ItemReceipt20(char *WareName, char *WareCode, char *Measure, char *SecID,
                int32_t Price, int32_t Count, int16_t WareType,
                int8_t ItemType, int8_t PayItemType, int8_t AgentType);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ItemReceiptCustom(char *WareName, char *WareCode, char *Measure, char *SecID, char *FreeField,
                    int8_t WareType, int8_t ComissType, int8_t TaxId, int32_t Price, int32_t Count, int32_t Total,
                    int32_t TaxValue, int32_t Comission, int32_t Percent, int32_t TotalComiss);
                

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ItemReceiptPlus(char *WareName, char *WareCode, char *Measure, char *SecID, char *FreeField,
                int32_t Price, int32_t Count, int16_t WareType);
                
                
                
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ItemReceiptPlus20(char *WareName, char *WareCode, char *Measure, char *SecID, char *FreeField,
                int32_t Price, int32_t Count, int16_t WareType,
                int8_t ItemType, int8_t PayItemType, int8_t AgentType);

                
                
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ItemReceiptEx(char *WareName, char *WareCode, char *Measure, char *SecID, char *FreeField,
                char *Price, int32_t Count, int16_t WareType);
                
                
                
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ItemReceiptEx20(char *WareName, char *WareCode, char *Measure, char *SecID, char *FreeField,
                char *Price, int32_t Count, int16_t WareType,
                int8_t ItemType, int8_t PayItemType, int8_t AgentType);
                
                
                
                
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ItemReceiptExx(char *WareName, char *WareCode, char *Measure, char *SecID, char *FreeField,
                char *Price, char *Count, int16_t WareType);
                

                
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ItemReceiptExx20(char *WareName, char *WareCode, char *Measure, char *SecID, char *FreeField,
                char *Price, char *Count, int16_t WareType,
                int8_t ItemType, int8_t PayItemType, int8_t AgentType);
                

                
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 void AZQT_AFTER_TYPE
LibEnable(int8_t IsEnable);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
LineReceiptNF(char *Line);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
LinesReceiptNF(char *Line1, char *Line2, char *Line3, char *Line4, char *Line5,
                char *Line6, char *Line7, char *Line8, char *Line9, char *Line10);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 void AZQT_AFTER_TYPE
LogEnable(int8_t IsEnable);



// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ofdAddPay(int32_t Index, char *Name);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ofdAddTax(int32_t Index, char *Name, int64_t Percent);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ofdCloseReceipt(char *TicketNumber);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ofdCloseShift(void);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ofdComissionReceipt(int64_t OType, int64_t ComissionTotal);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ofdCloseInstance(void);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ofdCreateInstance(int32_t ID, char *Token, char *serverHost, int32_t ServerPort, int32_t timeOut);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ofdFromCash(int64_t Sum);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
OFDGetParam(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
OFDGetPaymentState(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
OFDGetTime(void);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ofdItemReceipt(char *WareName, char *SecID, int32_t Price, int32_t Count, int32_t TotalItem);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
OFDSetParam(char *Addr, int16_t Port, int16_t Timeout, int16_t TimeoutRead, int16_t TimeoutWrite);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
OFDSetTime(void);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ofdStartReceipt(char *Operator, int8_t DocType);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ofdTaxReceipt(int32_t Index, int64_t Sum, int64_t Percent);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ofdTenderReceipt(int8_t PayType, int64_t TenderSum);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ofdToCash(int64_t Sum);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ofdTotalReceipt(int32_t Total);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ofdXReport(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
OpenDLL(char *OpName, char *Psw, char *DevName, int32_t FlagOem);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
OpenDLLEx(char *OpName, char *Psw, char *DevName, char *BaudRate, int32_t FlagOem);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
OpenDLLPlus(char *OpName, char *Psw, char *DevName, char *BaudRate, int32_t FlagOem);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
OpenFDoc(void);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
OpenFiscalDoc(int8_t DocType, int8_t PayType, int8_t FlipFOffs, int8_t PageNum, int8_t HCopyNum,
              int8_t VCopyNum, int16_t LOffs, int16_t VGap, int8_t LGap, int32_t Sum);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
OpenFiscalDoc77(int8_t DocType, int8_t PayType, int8_t FlipFOffs, int8_t PageNum,
                int8_t HCopyNum, int8_t VCopyNum, int16_t LOffs, int16_t VGap, int8_t LGap,
                int32_t Sum);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
OpenFiscalDocEx(int8_t DocType, int8_t PayType, int8_t FlipFOffs, int8_t PageNum, int8_t HCopyNum,
              int8_t VCopyNum, int16_t LOffs, int16_t VGap, int8_t LGap, char *Sum);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
OpenFiscalDocEx77(int8_t DocType, int8_t PayType, int8_t FlipFOffs, int8_t PageNum,
                int8_t HCopyNum, int8_t VCopyNum, int16_t LOffs, int16_t VGap, int8_t LGap,
                char *Sum);


                
// NOT IMPLEMENTED                
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
OpenFiscalDocPlus(int8_t DocType, int8_t FlipFOffs, int8_t PageNum, int8_t HCopyNum, int8_t VCopyNum,
                    int16_t LOffs, int16_t VGap, int8_t LGap, int8_t DepartNum, int8_t ArticlesNum, int32_t Sum);
                
                
                
// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
OpenFiscalDocPlusEx(int8_t DocType, int8_t FlipFOffs, int8_t PageNum, int8_t HCopyNum, int8_t VCopyNum,
                    int16_t LOffs, int16_t VGap, int8_t LGap, int8_t DepartNum, int8_t ArticlesNum, char *Sum);
                
                
                
                

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
OpenTCPDLL(char *OpName, char *Psw, char *ServerName, int32_t Port, int32_t FlagOem);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
PDFPaint(int8_t Options, int8_t Column, int32_t Ratio, char *PDF417);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
PDFPrint(int8_t Width, int8_t Height, int8_t isClear);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
PDFReport(int32_t No);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
PrintBarcodeFDoc(int8_t BType, int8_t BWidth, int8_t BHight, int8_t HRIFont,
                    int8_t HRIMode, int8_t BarCodeLen, char *BarCode);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
PrintEjournal(void);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
PrintFDoc(char *Information, int16_t Len);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
PrintFiscalReceipt(void);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
PrintFiscalReceiptPlus(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
PrintFiscalSlip(void);

// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
PrintFiscalSlip77(void);



// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
PrintFiscalSlipPlus(void);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
PrintHFDoc(char *H1, char *H2, char *H3, char *H4, char *H5, char *H6, int8_t IsCut);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
PrintHFDocEx(char *H1, char *H2, char *H3, char *H4, char *H5, char *H6, int8_t IsCut, int8_t IsGraph);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
PrintHFDocPlus(char *H1, char *H2, char *H3, char *H4, char *H5, char *H6, int8_t IsCut, int8_t IsGraph, int8_t LineNum);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
PrintLogo(int8_t INum);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
PrintOEMCRLFDoc(char *Information, int16_t Len);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
PrintOEMDoc(char *Information, int16_t Len);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
QRPaint(int8_t Options, int8_t Width, char *QR);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
QRPrint(int8_t Width, int8_t Height, int8_t isClear);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ReadCMOS(int8_t Offs, int8_t Num);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ReadComm(char *P, int32_t Count);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ReadIniString(const char *inistr);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ReadPrinterBuf(char *pBuf);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ReadPrinterBufCount(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ReStart(void);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
RunCashCode(int8_t Cmd);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
Sertification(char *Serial);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetArt(int32_t DepNum);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetAutoID(int8_t IsIDAuto);



// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetBillType(int8_t tMask, int32_t tLimit);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetCashCode(int8_t tCmd, int8_t tChannel);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetCommParam(char *DCB);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetCommTimeout(int32_t WaitRXTime, int32_t WaitTXTime);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetCommTimeoutMs(int32_t WaitRXTime, int32_t WaitTXTime);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetCOMPort(int8_t COM);



// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetCurrency(int8_t Index, char *CName, int8_t Prec, int8_t PPrec);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetDate(void);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetDateEx(char *CurDate, char *CurTime);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 void AZQT_AFTER_TYPE
SetDecimalPoint(int8_t IsEnable);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetDept(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetDrawerParam(int8_t OnTime, int8_t OffTime, int16_t ParamDoc1);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetDTR(int8_t DTRControl);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetDTRControl(int8_t DTRControl);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetECRTCP(int8_t isIPAvto, int8_t isDHCPAvto, int32_t IP, int32_t Mask, int32_t Gate,
            int32_t DNS1, int32_t DNS2, char *MAC);
            


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetHeader(char *H1, char *H2, char *H3, char *H4);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetHeaderNew(char *H1, char *H2, char *H3, char *H4, char *H5, char *H6);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetIDChar(char CharID);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetInterfaceParam(char *sBaudRate, int8_t sIs5Wires, int8_t sIsDateTime);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetOFDConnStatus(int8_t isOn);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetOFDConnStataus(int8_t isOn);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetOFDMessage(char *pType, char *pMessage);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetOperNames(char *N1, char *N2, char *N3, char *N4, char *N5, char *N6, int32_t NCmd);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetParamDoc(int16_t ParamDoc1, int16_t ParamDoc2, int16_t TimeoutSlip);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetPassword(char *Psw);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetPayment(int8_t Index, char *PName, int32_t IsSecondLine, int32_t IsChange, int8_t CurrencyIndex,
            int8_t PermOperation, char *CrossCourse);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetPrezenter(int8_t IsRetrak, int8_t IsPrezenter, int8_t IsSet);



// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetShiftCloseTime(char *P);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetSubParam(char *pSubParam);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetTail(char *T1, char *T2, char *T3, char *T4);



// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetTaxes(int8_t TIndex, int8_t TType, char *TName, char *TValue, char *TMin, int16_t TTeg);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 void AZQT_AFTER_TYPE
SetTCPDelay(int32_t TCPTime);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SetWEB(char *WEB);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ShiftClose(void);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ShiftCloseEx(int8_t IsPrint);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ShiftCloseFile(char *FileName, int32_t IsZNeeded);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ShiftClosePDFReport(int32_t No);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ShiftClosePlus(char *Cashier, char *OFDMessage, char *SubParam, int8_t IsPrint);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ShiftOpen(char *Buf);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ShiftOpenEx(char *CurDate, char *CurTime, char *Buf);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ShiftOpenPlus(char *Cashier, char *OFDMessage, char *SubParam, char *Buf);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ShowDisplay(char *Info, int32_t Len);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SlipEjectFDoc(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SlipSelectFDoc(void);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
StartMonitor(void);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
StartReceipt(int8_t DocType, int8_t Copies, int8_t Taxation, char *TableNo, char *PlaceNo, char *AccountNo);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
StartReceiptNF(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
StartReceiptPlus(int8_t DocType, int8_t Copies, int8_t Taxation, char *TableNo, char *PlaceNo,
                char *AccountNo, char *FreeField);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
StartSeans(void);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SubTotalReceipt(void);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
SubTotalReceiptPlus(char *FreeField);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
TaxReceipt(int8_t TaxIndex);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
TaxReceiptPlus(int8_t TaxIndex, char *FreeField);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
TenderReceipt(int8_t PayType, int32_t TenderSum, char *CardName);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
TenderReceiptEx(int8_t PayType, char *TenderSum, char *CardName, char *FreeField);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
TenderReceiptPlus(int8_t PayType, int32_t TenderSum, char *CardName, char *FreeField);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
TerminateMonitor(void);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ToCash(int32_t Sum);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ToCashEx(char *Sum, char *FreeField);

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
ToCashPlus(int32_t Sum, char *FreeField);



AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
TotalReceipt(void);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
TotalReceiptCustom(int32_t CustomTotal,
        int32_t TaxIndex1, int32_t TaxSum1,
        int32_t TaxIndex2, int32_t TaxSum2,
        int32_t TaxIndex3, int32_t TaxSum3,
        int32_t TaxIndex4, int32_t TaxSum4,
        int32_t TaxIndex5, int32_t TaxSum5,
        int32_t TaxIndex6, int32_t TaxSum6,
        int32_t TaxIndex7, int32_t TaxSum7,
        int32_t TaxIndex8, int32_t TaxSum8);
        

AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
TotalReceiptPlus(char *FreeField);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
UPDOn(int8_t isOn);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
VirtualPaperLimitFDoc(int8_t H, int8_t L);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
WriteCMOS(int8_t Offs, char *Info);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
WriteComm(char *P, int32_t Count);



// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
XmlForm(char *fName);


// NOT IMPLEMENTED
AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
XmlFormPlus(char *fName);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 void AZQT_AFTER_TYPE
XonXoffEnable(bool IsEnable);


AZQT_BFR_1 AZQT_BFR_2 AZQT_BFR_3 int32_t AZQT_AFTER_TYPE
XReport(void);


//---------------------------------------
#endif
//---------------------------------------









