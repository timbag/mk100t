#include "mainwindow.h"

#include <QApplication>
#include <QTextCodec>
#include "QtConsole_/include/qconsole.h"


QTextCodec *cp1251;
QTextCodec *cp866;
QTextCodec *utf8;
QTextCodec *cpcurrent;
QString tempqstr;
QTextStream tempqstream(&tempqstr);
QStringList paths;

int main(int argc, char *argv[])
{
    //qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));

    QApplication a(argc, argv);

    cp1251 = QTextCodec::codecForName("Windows-1251");
    cp866 = QTextCodec::codecForName("Windows-1251");
    utf8 = QTextCodec::codecForName("UTF-8");
    cpcurrent = cp1251;

    MainWindow w;
    w.showFullScreen();
    w.update();

    return a.exec();
}
