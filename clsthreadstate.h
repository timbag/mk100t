#ifndef CLSTHREADSTATE_H
#define CLSTHREADSTATE_H

#include <QObject>
#include <QString>
#include <QThread>
#include <QProcess>
#include <QDebug>

class clsThreadState : public QThread
{
    Q_OBJECT
public:
    clsThreadState(QString name);
    void run();

signals:
    void send(QString);
private:
    QString name;
    QString shell_command(const char *input, int timeout = -1, char isErrorRes = 0x00);
};

class clsThreadBT: public QThread
{
    Q_OBJECT
public:
    clsThreadBT(QString name);
    void run();

signals:
    void send(QString);
private:
    QString name;
    QString shell_command(const char *input, int timeout = -1, char isErrorRes = 0x00);
};

class clsThreadGPS: public QThread
{
    Q_OBJECT
public:
    clsThreadGPS(QString name);
    void run();

signals:
    void send(QString);
private:
    QString name;
    QString shell_command(const char *input, int timeout = -1, char isErrorRes = 0x00);
};

class clsThread4G: public QThread
{
    Q_OBJECT
public:
    clsThread4G(QString name);
    void run();

signals:
    void send(QString);
private:
    QString name;
    QString shell_command(const char *input, int timeout = -1, char isErrorRes = 0x00);
};

#endif // CLSTHREADSTATE_H
